//
//  NotificationViewController.swift
//  OneSignalNotificationContentExtension
//
//  Created by Daniel Weber on 29.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import OneSignal

struct Identifiers {
    static let reminderCategory = "reminder"
    static let cancelAction = "snooze_id"
    static let createAction = "create_id"
}

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    @IBOutlet weak var labelBottom: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    let appId = "92c13ef8-f374-47a1-95f6-a2e470eec1b2"
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var receivedRequest: UNNotificationRequest!
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCategory()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.body
        if let spiele = notification.request.content.userInfo["spiele"] as? [NSDictionary]{
            switch notification.request.content.categoryIdentifier{
                case "reminder":
                    if spiele.count == 1{
                        self.label?.text = "Unterwegs zum Spiel"
                    }else{
                        self.label?.text = "Unterwegs zu einem der folgenden Spiele?"
                    }
                    var text = ""
                    for dict in spiele{
                        if let spiel = Spiel.init(dictionary: dict), spiel.heim != nil, spiel.mannschaft != nil{
                            text += "\(spiel.mannschaft!) um \(spiel.zeit ?? "0:00") vs \(spiel.heim!)\n"
                        }
                        
                    }
                    self.textView?.text = text
                    self.labelBottom.text = "Dann trag doch schon jetzt deine Fahrtkosten ein."
                case "back_from_match":
                    if spiele.count == 1{
                        self.label?.text = "Du kommst zurück vom Spiel "
                    }else{
                        self.label?.text = "Du kommst zurück von einem der Spiele "
                    }
                    var text = ""
                    for dict in spiele{
                        if let spiel = Spiel.init(dictionary: dict), spiel.heim != nil, spiel.mannschaft != nil{
                            text += "\(spiel.mannschaft!) vs \(spiel.heim!)\n"
                        }
                        
                    }
                    self.textView?.text = text
                    self.labelBottom.text = "Dann trag jetzt die Fahrtkosten dafür ein!"
                
            default:
                break
            }
            
        }
    }
    
    func didReceive(_ response: UNNotificationResponse,
                    completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
        
        if response.actionIdentifier == "snooze_id" {
            let request = response.notification.request
            
            let identifiers = [request.identifier]
            
            // Remove future notifications that have been scheduled
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
            
            // Remove any notifications that have already been delivered so we're not cluttering up the user's notification center
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: identifiers)
            
            // Visual feedback that notification has been cancelled
            
            resendReminder()
            
            completion(.dismiss)
        }
        else {
            completion(.dismissAndForwardAction)
        }
        
    }
    
    private func resendReminder(){
        let defaults = UserDefaults.init(suiteName: "group.de.tvbeerfelden.fahrtkosten")
        let playerID = defaults?.value(forKey: "playerId") as? String
        
        let notificationData: Dictionary<String, Any> = [
            "app_id": appId,
            //"included_segments": ["All"],
            "include_player_ids": [playerID],
            "template_id": "e9868011-25b3-457f-87a5-61fa5625d243",
            "send_after": getDateString(Date(timeIntervalSinceNow: 10))
        ]
        OneSignal.postNotification(notificationData)
    }
    
    private func getDateString(_ date: Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZ"
        //print(formatter.string(from: date))
        return formatter.string(from: date)
    }
    
    func addCategory() {
        // Add action
        let cancelAction = UNNotificationAction(identifier: Identifiers.cancelAction,
                                                title: "Später erinnern",
                                                options: [.destructive])
        let createAction = UNNotificationAction(identifier: Identifiers.createAction, title: "Fahrtkosten eintragen", options: [.foreground])
        
        // Create category
        let category = UNNotificationCategory(identifier: Identifiers.reminderCategory,
                                              actions: [cancelAction, createAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }

}
