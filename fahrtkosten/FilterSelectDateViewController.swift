//
//  FilterSelectDateViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 26.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import NotificationCenter

class FilterSelectDateViewController: UIViewController{
    
    // MARK: Attributes
    var filterType: FilterType?

    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let filter = myFilters.first(where: {$0.filterType == self.filterType}), let searchDate = filter.searchDate{
            datePicker.setDate(searchDate, animated: true)
        }

        // Do any additional setup after loading the view.var  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        if let filter = myFilters.first(where: {$0.filterType == self.filterType}){
            filter.searchDate = sender.date
        }else if let newFilter = FilterElement.init(filterType: self.filterType!, searchString: nil, searchDate: sender.date){
            myFilters.append(newFilter)
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue:"filtersChanged"), object: nil)
    }
    
    @IBAction func clearDateFilter(_ sender: Any) {
        if let index = myFilters.index(where: {$0.filterType == self.filterType}){
            myFilters.remove(at: index)
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
