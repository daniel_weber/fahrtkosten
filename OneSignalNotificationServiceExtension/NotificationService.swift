//
//  NotificationService.swift
//  OneSignalNotificationServiceExtension
//
//  Created by Daniel Weber on 28.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            let userInfo = bestAttemptContent.userInfo as! [String:Any]
            //change the subtitle
            if let spiele = userInfo["spiele"] as? [NSDictionary]{
                var text = ""
                if spiele.count == 1{
                    text = "Du kommst zurück vom Spiel "
                    if let spiel = Spiel(dictionary: spiele[0]), let heim = spiel.heim{
                        text += "gegen "+heim
                    }
                }else{
                    text = "Du kommst zurück von einem der Spiele "
                    for dict in spiele{
                        if let spiel = Spiel.init(dictionary: dict), spiel.heim != nil, spiel.mannschaft != nil{
                            text += "\(spiel.mannschaft!) gegen \(spiel.heim!)\n"
                        }
                    }
                }
                
                bestAttemptContent.body = text
                //bestAttemptContent.subtitle = userInfo["subtitle"] as! String
            }
            //change the content to the order
            
            
            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    func changePizzaNotification(content oldContent:UNNotificationContent) -> UNMutableNotificationContent{
        let content = oldContent.mutableCopy() as! UNMutableNotificationContent
        //get the dictionary
        let userInfo = content.userInfo as! [String:Any]
        //change the subtitle
        if let subtitle = userInfo["subtitle"]{
            content.subtitle = subtitle as! String
        }
        
        //change the body with the order
        if let spiele = userInfo["spiele"] as? [NSDictionary]{
            var text = ""
            if spiele.count == 1{
                text = "Du kommst zurück vom Spiel "
                if let spiel = Spiel(dictionary: spiele[0]), let heim = spiel.heim{
                    text += "gegen "+heim
                }
            }else{
                text = "Du kommst zurück von einem der Spiele "
                for dict in spiele{
                    if let spiel = Spiel.init(dictionary: dict), spiel.heim != nil, spiel.mannschaft != nil{
                        text += "\(spiel.mannschaft!) gegen \(spiel.heim!)\n"
                    }
                }
            }
            
            content.body = text
            //bestAttemptContent.subtitle = userInfo["subtitle"] as! String
        }
        return content
    }
}
