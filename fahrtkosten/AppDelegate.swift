//
//  AppDelegate.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 07.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces
import OneSignal
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    var isGrantedNotificationAccess:Bool = false


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let returnValue = FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        ( window?.rootViewController as! UITabBarController ).tabBar.tintColor = UIColor.red
        ( window?.rootViewController as! UITabBarController ).tabBar.unselectedItemTintColor = UIColor.darkGray
        
        currentUser = loadUser()
        
        if let token = AccessToken.current{
            WebService.shared.setAccessToken(token: token)
        }else if let token = WebService.shared.getSecureString(forKey: "authenticationToken"), let expiresAt = WebService.shared.getSecureString(forKey: "expiringDate"){
            WebService.shared.setAccessToken(token: token, expiresAt: expiresAt)
        }else{
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.barTintColor = UIColor.red
        navigationBarAppearace.tintColor = UIColor.white
        
        // Google Maps
        GMSServices.provideAPIKey("AIzaSyAKvB3nlV7AHd-Id39dDxRN2YLfzRgIelA")
        GMSPlacesClient.provideAPIKey("AIzaSyAKvB3nlV7AHd-Id39dDxRN2YLfzRgIelA")
        
        // One Signal
        
        OneSignal.add(OneSignalHelper.shared as OSSubscriptionObserver)
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            print("Received Notification: \(notification!.payload.notificationID)")
            if notification!.payload.contentAvailable {
                
                if let template = notification!.payload.additionalData["template"] as? String{
                    switch template{
                        case "unterwegsZuTreffpunkt":
                        self.scheduleNotification(at: Date.init(timeIntervalSinceNow: 60), userInfo: nil)
                    default:
                        break
                    }
                }
                
            }
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            var fullMessage = payload.body
            
            //Try to fetch the action selected
            if let additionalData = payload.additionalData, let actionSelected = additionalData["actionSelected"] as? String {
                fullMessage =  "\(fullMessage)\nPressed ButtonId:\(actionSelected)"
                
                if actionSelected == "snooze_id", let template_Id = payload.additionalData["template"] as? String{
                    OneSignalHelper.shared.resendNotification(template: template_Id, date: Date(timeIntervalSinceNow: 10))
                
                }
            }
            print("fullMessage = \(fullMessage)")
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true,
                                     kOSSettingsKeyInAppLaunchURL: true]

        
        //let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: OneSignalHelper.shared.appId,
                                        handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        OneSignal.promptLocation()
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
       
        /*OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })*/
        
        // Sync hashed email if you have a login system or collect it.
        //   Will be used to reach the user at the most optimal time of day.
        // OneSignal.syncHashedEmail(userEmail)
        
        // UserNotification
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in
                self.isGrantedNotificationAccess = granted
        }
        )
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        let stried = CLLocationCoordinate2D(latitude: 49.564253, longitude: 8.973859)
        
        GeoLocationHelper.shared.startMonitoring(location: stried, radius: 100, identifier: "stried")
        
        return returnValue
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
         NSKeyedArchiver.archiveRootObject(currentUser, toFile: User.ArchiveURL.path)
        self.saveContext()
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb{
            let webURL = userActivity.webpageURL!
            
            present(URL: webURL as NSURL)
        }
        
        return true
    }
    
    private func present(URL url: NSURL)-> Bool{
        
        return false
    }
    // Support for background fetch
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
        self.getSpieleForMonitoring(completion: {plaene in
            GeoLocationHelper.shared.stopMonitoring()
            for plan in plaene{
                for spiel in plan.spiele{
                   GeoLocationHelper.shared.setMonitoringForMatch(spiel)
                }
            }
        })
    }
    
    // MARK: Remote Notification
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Update the app interface directly.
        
        // Play a sound.
        completionHandler(UNNotificationPresentationOptions.sound)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        //completionHandler()
        if response.actionIdentifier == "create_id"{
            var spieleToPass = [Spiel]()
            if let spiele = response.notification.request.content.userInfo["spiele"] as? [NSDictionary]{
                for dict in spiele{
                    if let spiel = Spiel.init(dictionary: dict), spiel.heim != nil, spiel.mannschaft != nil{
                        spieleToPass.append(spiel)
                    }
                }
            }
            self.redirectToPage(spiele: spieleToPass)
        }
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        completionHandler()
    }
    
    // triggered when content-available is set to true
    /*func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
        print("recieved silent push")
        self.scheduleNotification(at: Date.init(timeIntervalSinceNow: 62), userInfo: nil)

    }*/
    
    // MARK: One Signal
    
    // MARK: Local Notifications
    
    func scheduleNotification(at date: Date, userInfo: Dictionary<AnyHashable, Any>?) {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(in: .current, from: date)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        
        let content = UNMutableNotificationContent()
        content.title = "Unterwegs zum Spiel?"
        content.body = "Wähle das Spiel zu dem du fährst, um die Fahrtkosten einzutragen"
        content.sound = UNNotificationSound.default()
        content.setValue("YES", forKeyPath: "shouldAlwaysAlertWhileAppIsForeground")
        if userInfo != nil{
            content.userInfo = userInfo!
        }
        content.categoryIdentifier = "reminder"
        
        self.getSpiele(completion: {plaene in
            var spiele = [NSDictionary]()
            for plan in plaene {
                for spiel in plan.spiele{
                    spiele.append(spiel.dictionaryRepresentation())
                }
            }
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            if spiele.count>0{
                let userInfo = ["spiele":spiele]
                content.userInfo = userInfo
                let request = UNNotificationRequest(identifier: "reminder", content: content, trigger: trigger)
                print("added local notification to fire at: \(trigger.nextTriggerDate() ?? Date())")
                UNUserNotificationCenter.current().add(request) {(error) in
                    if let error = error {
                        print("Uh oh! We had an error: \(error)")
                    }
                }
            }
        })
        
        
    }
    
    func getSpieleForMonitoring(completion: @escaping ([Spielplan])->Void){
        WebService.shared.getSpiele(completion: {plaene in
            var spiele = [Spielplan]()
            
            for plan in plaene{
                let filteredSpiele = plan.spiele.filter({
                    if let date = $0.getDate(){
                        return date > Date.init(timeIntervalSinceNow: -60*60*12) && date < Date.init(timeIntervalSinceNow: 60*60*24*200) && !$0.isHeimspiel()
                    }
                    return false
                })
                let filtered = Spielplan(mannschaft: plan.mannschaft, spiele: filteredSpiele)
                spiele.append(filtered)
            }
            completion(spiele)
        })
    }
    
    func getSpiele(completion: @escaping ([Spielplan])->Void){
        WebService.shared.getSpiele(completion: {plaene in
            var spiele = [Spielplan]()
            
            for plan in plaene{
                let filteredSpiele = plan.spiele.filter({
                    if let date = $0.getDate(){
                        return date > Date() && date < Date.init(timeIntervalSinceNow: 60*60*3) && !$0.isHeimspiel()
                    }
                    return false
                })
                let filtered = Spielplan(mannschaft: plan.mannschaft, spiele: filteredSpiele)
                spiele.append(filtered)
            }
            completion(spiele)
        })
    }
    
    /*func redirectToPage(spiele: [Spiel]?)
    {
        var viewControllerToBrRedirectedTo: SpielPlanViewController?
        var mainNavigationController: MainViewController!
        mainNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabBarController") as? MainViewController
        viewControllerToBrRedirectedTo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "spielplanViewController") as? SpielPlanViewController
        if viewControllerToBrRedirectedTo != nil
        {
            viewControllerToBrRedirectedTo?.selectMatch = true
            viewControllerToBrRedirectedTo?.fromPush = true
            if spiele != nil{
                viewControllerToBrRedirectedTo?.spiele = spiele!
            }
            
            if self.window != nil && self.window?.rootViewController != nil
            {
                let rootVC = self.window?.rootViewController!
                if rootVC is UINavigationController
                {
                    (rootVC as! UINavigationController).pushViewController(viewControllerToBrRedirectedTo!, animated: true)
                }
                else
                {
                    self.window?.rootViewController! = mainNavigationController
                    if let vc = mainNavigationController.viewControllers?[1] as? SpielPlanViewController{
                        vc.selectMatch = true
                        vc.fromPush = true
                    }
                    mainNavigationController.selectedIndex = 1
                    
                }
                
                
            }
        }
        
    }*/
    
    func redirectToPage(spiele: [Spiel]?){
        
        if let navigationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "spielNavigationController") as? UINavigationController, let viewControllerToBrRedirectedTo =  navigationVC.topViewController as? SpielPlanViewController{
            viewControllerToBrRedirectedTo.selectMatch = true
            viewControllerToBrRedirectedTo.fromPush = true
            let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: viewControllerToBrRedirectedTo, action: #selector(viewControllerToBrRedirectedTo.dimissView))
            viewControllerToBrRedirectedTo.navigationItem.leftBarButtonItem = cancel
            if spiele != nil{
                viewControllerToBrRedirectedTo.spiele = spiele!
            }
            
            if let rootVC = self.window?.rootViewController{
               rootVC.present(navigationVC, animated: true, completion: nil)
            }
        }
        
    }
    

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "fahrtkosten")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func loadUser()->User?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: User.ArchiveURL.path) as? User
    }

}

