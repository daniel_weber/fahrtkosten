//
//  User.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 26.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import Foundation

var currentUser: User?
var userID:String?

enum Role:String{
    case admin = "admin"
    case author = "author"
    case readOnly = "readOnly"
}

class User: NSObject, NSCoding{
    var id: String?
    var fullName: String?
    var firstName: String?
    var email: String?
    var pictureUrl: String?
    var oneSignalId: String?{
        didSet{
            let defaults = UserDefaults.init(suiteName: "group.de.tvbeerfelden.fahrtkosten")
            defaults?.set(oneSignalId as Any, forKey: "playerId")
            defaults?.synchronize()
        }
    }
    var role:Role = .readOnly
    
    init?(id: String?, fullName: String?, email:String?){
        self.id = id
        self.fullName = fullName
        self.firstName = fullName?.characters.split(separator: " ").map(String.init)[0]
        self.email = email
    }
    
    init?(id: String?, fullName: String?, firstName:String?, email:String?, pictureUrl: String?){
        self.id = id
        self.fullName = fullName
        self.firstName = firstName
        self.email = email
        self.pictureUrl = pictureUrl
    }
    
    init?(id: String?, fullName: String?, firstName:String?, email:String?, pictureUrl: String?, oneSignalId: String?, role: Role){
        self.id = id
        self.fullName = fullName
        self.firstName = firstName
        self.email = email
        self.pictureUrl = pictureUrl
        self.oneSignalId = oneSignalId
        self.role = role
    }
    
    func setRole(_ role:Role){
        self.role = role
    }
    
    func setOneSignalId(oneSignalId: String?){
        self.oneSignalId = oneSignalId
    }
    
    struct PropertyKey {
        static let id = "id"
        static let fullName = "fullName"
        static let firstName = "firstName"
        static let email = "email"
        static let pictureUrl = "pictureUrl"
        static let role = "role"
        static let oneSignalId = "oneSignalId"
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: PropertyKey.id)
        aCoder.encode(firstName, forKey: PropertyKey.firstName)
        aCoder.encode(fullName, forKey: PropertyKey.fullName)
        aCoder.encode(email, forKey: PropertyKey.email)
        aCoder.encode(pictureUrl, forKey: PropertyKey.pictureUrl)
        aCoder.encode(role.rawValue, forKey: PropertyKey.role)
        aCoder.encode(oneSignalId, forKey: PropertyKey.oneSignalId)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        let id = aDecoder.decodeObject(forKey: PropertyKey.id) as? String
        let firstName = aDecoder.decodeObject(forKey: PropertyKey.firstName) as? String
        let fullName = aDecoder.decodeObject(forKey: PropertyKey.fullName) as? String
        let email = aDecoder.decodeObject(forKey: PropertyKey.email) as? String
        let pictureUrl = aDecoder.decodeObject(forKey: PropertyKey.pictureUrl) as? String
        let role =  Role.init(rawValue: aDecoder.decodeObject(forKey: PropertyKey.role) as! String)!
        let oneSignalId = aDecoder.decodeObject(forKey: PropertyKey.oneSignalId) as? String
        self.init(id: id, fullName: fullName, firstName: firstName, email: email, pictureUrl: pictureUrl, oneSignalId:oneSignalId, role: role)
    }
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("user")
    
}
