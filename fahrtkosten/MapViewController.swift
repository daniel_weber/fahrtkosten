//
//  MapViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 21.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import NotificationCenter

struct DestinationDistance{
    var mapItem: MKMapItem
    var distance: CLLocationDistance = 0
}

class MapViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate{
    
    // MARK: Attributes
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var searchBar = UISearchBar()
    let initialLocation = CLLocation(latitude: 49.5615995, longitude: 8.9687293)
    let regionRadius: CLLocationDistance = 1000
    
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    var selectedDestination: DestinationDistance?
    var destinationName: String?
    
    
    var firstLocationIsSet = false
    
    var searchResults = [MKMapItem]()
    var searchResultsDistance = [DestinationDistance]()
    
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.barTintColor = UIColor(rgb: 0x2dbecd)
        searchBar.searchBarStyle = .minimal
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        //searchbar.layer.borderWidth = 0
        //searchbar.layer.borderColor = UIColor.clear.cgColor
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        self.navigationBar.topItem?.titleView = searchBar
        mapView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        if selectedDestination != nil{
            self.firstLocationIsSet = true
            setMapview(item: selectedDestination!.mapItem, distance: selectedDestination!.distance)
        }else if self.destinationName != nil{
            self.firstLocationIsSet = true
            self.searchOnMap(str: self.destinationName!)
        }
        self.addStatusBarBG()
        finMyLocation()
        //centerMapOnLocation(location: initialLocation)
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // SearchBarDelegate
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchResultsDistance = []
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.frame.size = CGSize(width: self.tableView.frame.size.width, height: 0)
        })
        
        if let searchText = searchBar.text{
            searchOnMap(str: searchText)
        }
        self.isEditing = false
        self.searchBar.resignFirstResponder()
        self.view.resignFirstResponder()
    }
    
    //TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultsDistance.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mapItemCell", for: indexPath)
        cell.textLabel?.text = searchResultsDistance[indexPath.row].mapItem.name
        cell.detailTextLabel?.text = "\((searchResultsDistance[indexPath.row].distance/1000).roundTo(places: 0)) km"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.frame.size = CGSize(width: self.tableView.frame.size.width, height: 0)
        })
        setMapview(item: searchResultsDistance[indexPath.row].mapItem, distance: searchResultsDistance[indexPath.row].distance)
        
    }
    
    // LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if manager.location != nil && !firstLocationIsSet{
            centerMapOnLocation(location: manager.location!)
            firstLocationIsSet = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    // MARK: MapViewDelegate
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView{
            print(view.annotation?.title) // annotation's title
            print(view.annotation?.subtitle) // annotation's subttitle
            
            if let userLocation = view.annotation as? MKUserLocation, let coordinate = userLocation.location{
                var geoCoder = CLGeocoder()
                geoCoder.reverseGeocodeLocation(coordinate, completionHandler: {placemarks, error in
                    if let placemark = placemarks?.first{
                        self.getDistance(placemark: placemark, completion: {distance in
                            let mkPlacemark = MKPlacemark(placemark: placemark)
                            self.selectedDestination = DestinationDistance(mapItem: MKMapItem(placemark: mkPlacemark), distance: distance)
                            self.dismissView(control)
                        })
                    }
                })
                
            }else{
               self.dismissView(control)
            }
            
            //Perform a segue here to navigate to another viewcontroller
            // On tapping the disclosure button you will get here
            
            
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseId = "pin"
        if annotation === self.pointAnnotation{
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
            
            if pinView == nil {
                //println("Pinview was nil")
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView!.canShowCallout = true
                pinView!.animatesDrop = true
            }
            
            let button1 = UIButton(type: .detailDisclosure)// button with info sign in it
            let button = UIButton(type: .custom)
            button.frame = button1.frame
            button.setImage(#imageLiteral(resourceName: "checkmark").withRenderingMode(.alwaysTemplate), for: .normal)
            button.setImage(#imageLiteral(resourceName: "checkmarkFilled").withRenderingMode(.alwaysTemplate), for: .selected)
            button.setImage(#imageLiteral(resourceName: "checkmarkFilled").withRenderingMode(.alwaysTemplate), for: .highlighted)
            button.tintColor = UIColor.red
            
            pinView?.rightCalloutAccessoryView = button
            
            
            return pinView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let ulav = mapView.view(for: mapView.userLocation) {
            let button1 = UIButton(type: .detailDisclosure)// button with info sign in it
            let button = UIButton(type: .custom)
            button.frame = button1.frame
            button.setImage(#imageLiteral(resourceName: "checkmark").withRenderingMode(.alwaysTemplate), for: .normal)
            button.setImage(#imageLiteral(resourceName: "checkmarkFilled").withRenderingMode(.alwaysTemplate), for: .selected)
            button.setImage(#imageLiteral(resourceName: "checkmarkFilled").withRenderingMode(.alwaysTemplate), for: .highlighted)
            button.tintColor = UIColor.red

            ulav.rightCalloutAccessoryView = button
        }
    }
    
    
    // MARK: Methods
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func searchOnMap(str: String){
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = str
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if let mapItems = localSearchResponse?.mapItems{
                if mapItems.count == 1{
                     self.getDistance(item: mapItems[0], addToTableView: false)
                }else{
                    for item in mapItems{
                        self.getDistance(item: item, addToTableView: true)
                    }
                }
                
            }else{
                self.searchResultsDistance = []
            }
        }
    }
    func setMapview(item: MKMapItem, distance: Double?){
        self.pointAnnotation = MKPointAnnotation()
        self.pointAnnotation.title = item.name
        if distance != nil{
            self.pointAnnotation.subtitle = "\(distance!/1000) km"
            self.pointAnnotation.subtitle = self.pointAnnotation.subtitle?.replacingOccurrences(of: ".", with: ",")
            selectedDestination = DestinationDistance(mapItem: item, distance: distance!)
        }
        self.pointAnnotation.coordinate = item.placemark.coordinate
        /*self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)*/
        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
        self.mapView.centerCoordinate = self.pointAnnotation.coordinate
        self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
        
        //let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
        let coordinate:CLLocationCoordinate2D = item.placemark.coordinate
        let span = MKCoordinateSpanMake(0.2, 0.2)
        let region = MKCoordinateRegionMake(coordinate, span)
        self.mapView.setRegion(region, animated: true)
        self.mapView.selectAnnotation(self.pointAnnotation, animated: true)
    }
    
    func finMyLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func getDistance(placemark:CLPlacemark, completion: @escaping(CLLocationDistance)->Void ){
        let request = MKDirectionsRequest()
        //if let location = self.locationManager.location
        if let location = initialLocation as? CLLocation{
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: location.coordinate))
            request.destination = MKMapItem(placemark: MKPlacemark(coordinate: placemark.location!.coordinate))
            request.transportType = .automobile
            
            let directions = MKDirections(request: request)
            directions.calculate(completionHandler: {(response, error) in
                if error != nil{
                    print(error)
                }
                if response != nil && response!.routes.count > 0{
                    if let distance = response?.routes[0].distance{
                        completion(distance)
                    }
                }
            })
        }
    }
    
    func getDistance(item : MKMapItem, addToTableView: Bool){
        let request = MKDirectionsRequest()
        //if let location = self.locationManager.location
        if let location = initialLocation as? CLLocation{
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: location.coordinate))
            request.destination = item
            request.transportType = .automobile
            
            let directions = MKDirections(request: request)
            directions.calculate(completionHandler: {(response, error) in
                if error != nil{
                    print(error)
                }
                if response != nil && response!.routes.count > 0{
                    var dist = DestinationDistance(mapItem: item, distance: 0)
                    if let distance = response?.routes[0].distance{
                        dist.distance = distance
                    }
                    if addToTableView {
                        DispatchQueue.main.async {
                            self.tableView.frame.size = CGSize(width: self.tableView.frame.size.width, height: CGFloat(integerLiteral: 44*self.searchResultsDistance.count+1))
                        }
                        let indexPath = IndexPath(row: self.searchResultsDistance.count, section: 0)
                        self.searchResultsDistance.append(dist)
                        self.tableView.insertRows(at: [indexPath], with: .top)
                    }else{
                        self.setMapview(item: item, distance: dist.distance)
                    }
                    
                    
                }
            })
        }
        
    }
    
    // MARK: Actions
    
    @IBAction func dismissView(_ sender: Any) {
        if self.selectedDestination != nil{
            let userInfo = [
                "destinationSelected": self.selectedDestination!
            ] as [String : Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "destinationSelected"), object: nil, userInfo: userInfo)
        }
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
