//
//  WebService.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 07.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import Foundation
import FacebookCore
//import Alamofire

class WebService: NSObject{
    
    private lazy var serviceRoot: String = {
        return "https://fahrtkosten.tvbeerfelden.de/v2"
    }()
    

    
    private var accessToken: AccessToken?
    private var authenticationToken: String?
    private var expiringDate: Date?
    
    static public let shared = WebService()
    
    
  
    
    
    // MARK: Methods
    
    func isLoggedIn()->Bool{
        if let token = self.accessToken{
            return token.expirationDate>Date()
        }else if let expiring = self.expiringDate{
            return expiring > Date()
        }
        return false
    }
    
    func setAccessToken(token: String, expiresAt: String){
        self.authenticationToken = token
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.expiringDate = formatter.date(from: expiresAt)!
    }
    
    func setAccessToken(token: AccessToken){
        self.accessToken = token
        self.authenticationToken = token.authenticationToken
    }
    
    func emailLogin(email: String, password: String, completion: @escaping (Bool, Any?)->Void){
        let urlStr = "\(serviceRoot)/emailLogin.php?login"
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = "POST"
        request.timeoutInterval = 180
        let loginData = [
            "username": email,
            "password": password
        ]
        request.httpBody = self.dicToJson(dic: loginData)
        let session = URLSession.shared
        let task = session.dataTask(with: request){ data, response, error in
            guard let data = data, error == nil else {
                completion(false, "error=\(error)" as AnyObject?)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 && httpStatus.statusCode != 201{
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                switch httpStatus.statusCode {
                case 401:
                    completion(false, "Falscher Benutzer oder falsches Passwort!" as Any?)
                default: completion(false, "Unknown error occurred" as Any?)
                }
            }
            print(String(data: data, encoding: .utf8))
            let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            completion(true, json as AnyObject?)
            
        }
        task.resume()
        
    }
    
    func emailLogout(_ completion: @escaping (Bool)->Void){
        let urlStr = "\(serviceRoot)/emailLogin.php?logout"
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = "GET"
        request.timeoutInterval = 180
        let session = URLSession.shared
        let task = session.dataTask(with: request){ _, response, error in
            guard error == nil else {
                completion(false)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                completion(false)
            }else{
                self.deleteSecureObject(forKey: "authenticationToken")
                self.deleteSecureObject(forKey: "expiringDate")
                currentUser = nil
                completion(true)
            }
        }
        task.resume()
    }
    
    func read(method: String, data: String?, parameters: [String:String], completion: @escaping (Bool, Any?) -> Void) {
        
        
        var urlStr = "\(serviceRoot)/webservice.php?token=\(self.authenticationToken ?? "")"
        
        for param in parameters{
            urlStr += "&\(param.key)=\(param.value)"
        }
        print("data to send:\(data)")
        print("url: \(urlStr)")
        var request = URLRequest(url: URL(string: urlStr)!)
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = method
        request.timeoutInterval = 180
        if data != nil{
            request.httpBody = data!.data(using: .utf8)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(false, "error=\(error)" as AnyObject?)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 && httpStatus.statusCode != 201{
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                switch httpStatus.statusCode {
                case 401:
                    completion(false, "Not Authorized" as Any?)
                default: completion(false, "Unknown error occurred" as Any?)
                }
            }
            let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            completion(true, json as AnyObject?)
            
        }
        task.resume()
        
    }
    
    func getFahrtkosten(completion: @escaping ([Fahrtkosten]) -> Void){
        var fahrtkosten = [Fahrtkosten]()
        read(method: "GET", data: nil, parameters: [:], completion: {(success, result) in
            if (success) {
                guard let root = result as? [String: Any], let results = root["posts"] as? [[String: Any]] else {
                    return
                }
                for record in results {
                    if let kosten = Fahrtkosten.init(dictionary: record as NSDictionary){
                        fahrtkosten.append(kosten)
                    }
                    
                }
                completion(fahrtkosten)
            }
        })
    }
    
    func createFahrtkosten(_ fahrtkosten: Fahrtkosten, completion: @escaping (Dictionary<String, String>?)->Void ){
        let data = dicToJson(dic: fahrtkosten.dictionaryRepresentation() as! Dictionary<String, String>)
        if let dataStr = String(data: data, encoding: .utf8){
            read(method: "POST", data: dataStr, parameters: [:], completion: {(success, result) in
                print("result:\(result)")
                guard let root = result as? [String: Any], let results = root["posts"] as? [[String: Any]] else {
                    return
                }
                completion(results[0] as! Dictionary<String, String>)
            })
        }else{
            print("Data could not be converted")
        }
        
    }
    
    func updateFahrtkosten(_ fahrtkosten: Fahrtkosten, completion: @escaping (Dictionary<String, String>?)->Void ){
        let data = dicToJson(dic: fahrtkosten.dictionaryRepresentation() as! Dictionary<String, String>)
        if let dataStr = String(data: data, encoding: .utf8){
            read(method: "PUT", data: dataStr, parameters: ["id":fahrtkosten.id!], completion: {(success, result) in
                print("result:\(result)")
                guard let root = result as? [String: Any], let results = root["posts"] as? [[String: Any]] else {
                    return
                }
                completion(results[0] as! Dictionary<String, String>)
            })
        }else{
            print("Data could not be converted")
        }
    }
    
    func deleteFahrtkosten(_ fahrtkosten: Fahrtkosten){
        read(method: "DELETE", data: nil, parameters: ["id":fahrtkosten.id!], completion: {(success, result) in
            print("result:\(result)")
        })
    }
    
    func archiveFahrtkosten(_ completion: @escaping (Bool, Any?)->Void){
        read(method: "DELETE", data: nil, parameters: ["all":""], completion: {(success, result) in
            print("result:\(result)")
            completion(success, result as Any)
        })
    }
    
    func getSpiele(completion: @escaping ([Spielplan]) -> Void){
        var request = URLRequest(url: URL(string: "\(serviceRoot)/getMatches.php")!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        request.timeoutInterval = 180
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            print(error)
            return
        }
        print(data)
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 && httpStatus.statusCode != 201{
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            switch httpStatus.statusCode {
                case 401:
                    print("Not Authorized")
                default: print("Unknown error occurred")
            }
        }
            var spielPlaene = [Spielplan]()
        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            guard let results = json as? [[String: Any]] else {
                return
            }
            for record in results {
                if let matches = record["spiele"] as? [[String: Any]], let mannschaft = record["mannschaft"] as? String{
                    var spiele = [Spiel]()
                    for match in matches{
                        if let spiel = Spiel.init(dictionary: match as NSDictionary){
                            spiele.append(spiel)
                        }
                    }
                    spielPlaene.append(Spielplan(mannschaft: mannschaft, spiele: spiele))
                }
                
            }
            completion(spielPlaene)
        }
        task.resume()
    }
    
    func setOneSignalId(oneSignalID: String){
        let dict = ["oneSignalId":oneSignalID]
        let data = dicToJson(dic: dict)
        let dataStr = String(data: data, encoding: .utf8)
        read(method: "POST", data: dataStr, parameters: ["oneSignal":""], completion: {_ in})
    }
    
    func getOneSignalId(completion: @escaping (String?)->Void){
        read(method: "GET", data: nil, parameters: ["oneSignal":""], completion: {(success, result) in
            print("result:\(result)")
            guard let root = result as? [String: Any], let results = root["posts"] as? [[String: Any]] else {
                return
            }
            completion(results[0]["onesignalId"] as? String)
        })
    }

    func dicToJson(dic: Dictionary<String, String>) -> Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            print(String(data: jsonData, encoding: .utf8))
            return jsonData
        } catch {
            print(error.localizedDescription)
            return "{}".data(using: .utf8)!
        }
    }
    
    func getSecureString(forKey: String) -> String? {
        return KeychainWrapper.standard.string(forKey: forKey)
    }
    
    
    func saveSecureString(_ value: String, forKey key: String) {
        KeychainWrapper.standard.set(value, forKey: key)
    }
    
    func deleteSecureObject(forKey key: String) {
        KeychainWrapper.standard.removeObject(forKey: key)
    }
    
}
