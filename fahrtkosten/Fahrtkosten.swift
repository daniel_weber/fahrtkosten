//
//  Fahrtkosten.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 07.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import Foundation
import UIKit


/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Fahrtkosten {
    public var id : String?
    public var datum : String?
    public var mannschaft : String?
    public var ort : String?
    public var bus : String?
    public var fahrer : String?
    public var fahrer2 : String?
    public var fahrer3 : String?
    public var entfernung : String?
    public var kosten : String?
    public var gesamtkosten : String?
    public var autor_id : String?
    public var browser : String?
    public var version : String?
    public var lang : String?
    public var platform : String?
    public var email: String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Fahrtkosten]
    {
        var models:[Fahrtkosten] = []
        for item in array
        {
            models.append(Fahrtkosten(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? String
        datum = dictionary["datum"] as? String
        mannschaft = dictionary["mannschaft"] as? String
        ort = dictionary["ort"] as? String
        bus = dictionary["bus"] as? String
        fahrer = dictionary["fahrer"] as? String
        fahrer2 = dictionary["fahrer2"] as? String
        fahrer3 = dictionary["fahrer3"] as? String
        entfernung = dictionary["entfernung"] as? String
        kosten = dictionary["kosten"] as? String
        gesamtkosten = dictionary["gesamtkosten"] as? String
        autor_id = dictionary["autor_id"] as? String
        browser = dictionary["browser"] as? String
        version = dictionary["version"] as? String
        lang = dictionary["lang"] as? String
        platform = dictionary["platform"] as? String
        email = dictionary["email"] as? String
    }
    
    init?(datum:Date?, mannschaft:String?, ort: String?, bus: Bool, fahrer1: String?, fahrer2:String?, fahrer3:String?, entfernung: String?, kosten: String?, gesamtkosten: String?){
        
        self.id = "99999"
        if datum != nil{
            setDate(datum!)
        }
        self.mannschaft = mannschaft
        self.ort = ort
        if bus{
            self.bus = "Ja"
        }else{
            self.bus = "Nein"
        }
        self.fahrer = fahrer1
        self.fahrer2 = fahrer2
        self.fahrer3 = fahrer3
        self.entfernung = entfernung
        self.kosten = kosten
        self.gesamtkosten = gesamtkosten
        self.autor_id = userID
        self.browser = UIDevice.current.model
        self.platform = UIDevice.current.systemName
        self.version = UIDevice.current.systemVersion
        self.lang = NSLocale.preferredLanguages.first
        self.email = currentUser?.email
    }
    
    func getDate()->Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if self.datum != nil {
            return formatter.date(from: self.datum!)
        }
        return nil
        
    }
    
    func setDate(_ date:Date){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
         self.datum = formatter.string(from: date)
    }
    
    func getFormattedDate()->String?{
        let date = getDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        if date != nil{
            let dateString = formatter.string(from: date!)
            return dateString
        }
        return ""
    }
    
    func updateFromDictonary(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? String
        datum = dictionary["datum"] as? String
        mannschaft = dictionary["mannschaft"] as? String
        ort = dictionary["ort"] as? String
        bus = dictionary["bus"] as? String
        fahrer = dictionary["fahrer"] as? String
        fahrer2 = dictionary["fahrer2"] as? String
        fahrer3 = dictionary["fahrer3"] as? String
        entfernung = dictionary["entfernung"] as? String
        kosten = dictionary["kosten"] as? String
        gesamtkosten = dictionary["gesamtkosten"] as? String
        autor_id = dictionary["autor_id"] as? String
        browser = dictionary["browser"] as? String
        version = dictionary["version"] as? String
        lang = dictionary["lang"] as? String
        platform = dictionary["platform"] as? String
        email = dictionary["email"] as? String
    }
    
    func create(){
        WebService.shared.createFahrtkosten(self, completion: { result in
            print(result)
            if let resultDict = result as NSDictionary?{
                self.updateFromDictonary(dictionary: resultDict)
            }
        })
    }
    
    func update(){
        WebService.shared.updateFahrtkosten(self, completion: { result in
            if let resultDict = result as NSDictionary?{
                self.updateFromDictonary(dictionary: resultDict)
            }
        })
    }
    
    func delete(){
        WebService.shared.deleteFahrtkosten(self)
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.datum, forKey: "datum")
        dictionary.setValue(self.mannschaft, forKey: "mannschaft")
        dictionary.setValue(self.ort, forKey: "ort")
        dictionary.setValue(self.bus, forKey: "bus")
        dictionary.setValue(self.fahrer, forKey: "fahrer")
        dictionary.setValue(self.fahrer2, forKey: "fahrer2")
        dictionary.setValue(self.fahrer3, forKey: "fahrer3")
        dictionary.setValue(self.entfernung, forKey: "entfernung")
        dictionary.setValue(self.kosten, forKey: "kosten")
        dictionary.setValue(self.gesamtkosten, forKey: "gesamtkosten")
        dictionary.setValue(self.autor_id, forKey: "autor_id")
        dictionary.setValue(self.browser, forKey: "browser")
        dictionary.setValue(self.version, forKey: "version")
        dictionary.setValue(self.lang, forKey: "lang")
        dictionary.setValue(self.platform, forKey: "platform")
        dictionary.setValue(self.email, forKey: "email")
        
        return dictionary
    }
    
}
