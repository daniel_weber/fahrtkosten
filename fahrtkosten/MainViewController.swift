//
//  MainViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 16.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import NotificationCenter
import OneSignal




class MainViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadData()
        self.getID()
        self.delegate = self
        
        self.tabBar.tintColor = UIColor.red
        self.tabBar.unselectedItemTintColor = UIColor.darkGray
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.barTintColor = UIColor.red
        navigationBarAppearace.tintColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TabView Delegate
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let tabViewControllers = tabBarController.viewControllers!
        guard let toIndex = tabViewControllers.index(of: viewController) else {
            return false
        }
        
        // Our method
        animateToTab(toIndex: toIndex)
        
        return true
    }
    
    // MARK: Methods
    
    //Data Related
    
    func loadData(){
       WebService.shared.getSpiele(completion: {spielplaen in
            spielplaene = spielplaen
        })
    }
    
    func getID(){
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me?fields=email,name,first_name,picture")) { httpResponse, result in
            switch result {
            case .success(let response):
                print(response)
                if let userID = response.dictionaryValue?["id"] as? String{
                    var picUrl: String? = nil
                    if let pictureFile = response.dictionaryValue?["picture"] as? [String: Any], let data = pictureFile["data"] as? [String: Any]{
                        picUrl = data["url"] as? String
                    }
                    currentUser = User(id: userID, fullName: response.dictionaryValue?["name"] as? String, firstName: response.dictionaryValue?["first_name"] as? String, email: response.dictionaryValue?["email"] as? String, pictureUrl: picUrl)
                    self.getRoles()
                    self.getOneSignalID()
                    OneSignal.promptForPushNotifications(userResponse: { accepted in
                        print("User accepted notifications: \(accepted)")
                    })
                }
            default:
                break
            }
        }
        connection.start()
    }
    
    func getOneSignalID(){
        WebService.shared.getOneSignalId(completion: {playerID in
            currentUser?.oneSignalId = playerID
        })
    }
    
    func getRoles(){
        
        let connection = GraphRequestConnection()
        let request = GraphRequest(graphPath: "/me/accounts")
        connection.add(request){ httpResponse, result in
            switch result {
            case .success(let response):
                if let data = response.dictionaryValue?["data"] as? [[String:Any]]{
                    for r in data{
                        if r["id"] as? String == "110703355663908"{
                            if let perms = r["perms"] as? [String]{
                                if perms.contains("ADMINISTER") || perms.contains("EDIT_PROFILE"){
                                    currentUser?.role = .admin
                                }else{
                                    currentUser?.role = .author
                                }
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updatedRoles"), object: nil)
                            }
                            break
                        }
                    }
                }
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    func animateToTab(toIndex: Int) {
        let tabViewControllers = viewControllers!
        let fromView = selectedViewController!.view
        let toView = tabViewControllers[toIndex].view
        let fromIndex = tabViewControllers.index(of: selectedViewController!)
        
        guard fromIndex != toIndex else {return}
        
        // Add the toView to the tab bar view
        fromView?.superview!.addSubview(toView!)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        let scrollRight = toIndex > fromIndex!;
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView?.center = CGPoint(x: fromView!.center.x + offset, y: toView!.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView?.center = CGPoint(x: fromView!.center.x - offset, y: fromView!.center.y);
            toView?.center   = CGPoint(x: toView!.center.x - offset, y: toView!.center.y);
            
        }, completion: { finished in
            
            // Remove the old view from the tabbar view.
            fromView?.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
