//
//  CreateFahrtkostenViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 20.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MapKit
import NotificationCenter
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import CoreLocation

class CreateFahrtkostenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: Attributes

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var pickerPlaceholderView: UIView!
    @IBOutlet weak var pickerWrapperView: UIView!
    var busSwitch: UISwitch?
    
    var placesClient: GMSPlacesClient!
    var locationManager : CLLocationManager!
    
    var editingEnabled = true
    var originalBarButton: UIBarButtonItem?
    
    var fahrtkosten: Fahrtkosten?
    
    var destinationName: String?{
        didSet{
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    var multiplikator:Double = 1{
        didSet{
             self.gesamtKosten = self.kosten * self.multiplikator
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    var destinationDistance: Double?{
        didSet{
            self.kosten = (destinationDistance!)*0.16
            self.gesamtKosten = self.kosten * self.multiplikator
        }
    }
    var selectedDestination: DestinationDistance?
    var kosten: Double = 0
    var gesamtKosten: Double = 0
    var mannschaft: String?{
        didSet{
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    var date: Date?{
        didSet{
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    var fahrer1:String?{
        didSet{
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    var fahrer2:String?{
        didSet{
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    var fahrer3:String?{
        didSet{
            if destinationName != nil && self.mannschaft != nil && self.date != nil && fahrer1 != nil && !fahrer1!.isEmpty && (multiplikator == 1 || (fahrer2 != nil && !fahrer2!.isEmpty && fahrer3 != nil && !fahrer3!.isEmpty)){
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
        }
    }
    
    var activeFieldFrame: CGRect?
    var originalInset: UIEdgeInsets?
    var pickerOrigin: CGRect?
    var tapAround:UITapGestureRecognizer?
    
    var mannschaftsPicker:UIPickerView?
    var datePicker:UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        //hideKeyboardWhenTappedAround()
        
        placesClient = GMSPlacesClient.shared()
        locationManager = CLLocationManager()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDestination(notification:)), name: NSNotification.Name(rawValue: "destinationSelected"), object: nil)
        
        if fahrtkosten != nil{
            self.destinationName = fahrtkosten?.ort
            if fahrtkosten?.bus == "Ja" || fahrtkosten?.bus == "ja"{
                self.multiplikator = 1
                self.busSwitch?.setOn(true, animated: true)
            }else{
                self.multiplikator = 3
                self.busSwitch?.setOn(true, animated: false)
                self.fahrer2 = fahrtkosten?.fahrer2
                self.fahrer3 = fahrtkosten?.fahrer3
            }
            self.fahrer1 = fahrtkosten?.fahrer
            if let entfernungStr = fahrtkosten!.entfernung, let entfernung = Double(entfernungStr){
              self.destinationDistance = entfernung
            }
            if let kostenStr = fahrtkosten!.kosten, let kosten = Double(kostenStr){
                self.kosten = kosten
            }
            if let gesamtKostenStr = fahrtkosten!.gesamtkosten, let gesamtKosten = Double(gesamtKostenStr){
                self.gesamtKosten = gesamtKosten
            }
            self.mannschaft = fahrtkosten?.mannschaft
            self.date = fahrtkosten?.getDate()
        }
        
        if !editingEnabled{
            let editButton = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(self.editButtonPressed))
            editButton.isEnabled = currentUser?.role == .admin || fahrtkosten?.email == currentUser?.email
            self.navigationItem.rightBarButtonItem = editButton
        }else{
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelButtonClicked(_:)))
            self.navigationItem.leftBarButtonItem = cancelButton
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
        self.pickerOrigin = pickerWrapperView.frame
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterFromKeyboardNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return "Spiel"
        case 1:
            return "Strecke"
        case 2:
            return "Fahrer"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            if self.destinationName != nil || self.selectedDestination != nil{
                return 4
            }
            return 3
        case 1:
            return 4
        case 2:
            return Int(multiplikator)
            /*if self.busSwitch != nil && !busSwitch!.isOn{
                return 3
            }else{
               return 1
            }*/
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        switch (indexPath.section, indexPath.row){
        case (0,0):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Mannschaft"
            cell.detailTextLabel?.text = self.mannschaft
            if self.editingEnabled{
                cell.accessoryType = .disclosureIndicator
                cell.selectionStyle = .default
            }else{
                cell.accessoryType = .none
                cell.selectionStyle = .none
            }
        case (0,1):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Datum"
            cell.detailTextLabel?.text = getFormattedDate(date: self.date)
            if self.editingEnabled{
                cell.accessoryType = .disclosureIndicator
                cell.selectionStyle = .default
            }else{
                cell.accessoryType = .none
                cell.selectionStyle = .none
            }
        case (0,2):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Ort"
            cell.detailTextLabel?.text = destinationName
            if self.editingEnabled{
                cell.accessoryType = .disclosureIndicator
                cell.selectionStyle = .default
            }else{
                cell.accessoryType = .none
                cell.selectionStyle = .none
            }
        case(0,3):
            cell = tableView.dequeueReusableCell(withIdentifier: "mapOnlyCell", for: indexPath) as! mapOnlyTableViewCell
            (cell as! mapOnlyTableViewCell).mapView.delegate = (cell as! mapOnlyTableViewCell)
            if let name = self.selectedDestination?.mapItem.name {
               (cell as! mapOnlyTableViewCell).setLocation(name)
            }else if self.destinationName != nil{
                (cell as! mapOnlyTableViewCell).setLocation(self.destinationName!)
            }
        case(1,0):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Entfernung"
            if self.destinationDistance != nil{
                let str = "\((self.destinationDistance!).roundTo(places: 2)) km"
                cell.detailTextLabel?.text = str.replacingOccurrences(of: ".", with: ",")
            }else{
                cell.detailTextLabel?.text = "0,00 km"
            }
            cell.accessoryType = .none
            cell.selectionStyle = .none
        case(1,1):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Kosten"
            cell.detailTextLabel?.text = self.kosten.toEuro()
            cell.accessoryType = .none
            cell.selectionStyle = .none
        case(1,2):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Gesamtkosten"
            cell.detailTextLabel?.text = self.gesamtKosten.toEuro()
            cell.accessoryType = .none
            cell.selectionStyle = .none
        case(1,3):
            cell = tableView.dequeueReusableCell(withIdentifier: "busCell", for: indexPath) as! busTableViewCell
            self.busSwitch = (cell as! busTableViewCell).busSwitch
            self.busSwitch?.addTarget(self, action: #selector(self.changeBusSwitch), for: .valueChanged)
            cell.selectionStyle = .none
            self.busSwitch?.isEnabled = self.editingEnabled
            if fahrtkosten?.bus == "nein" || fahrtkosten?.bus == "Nein"{
                self.busSwitch?.setOn(false, animated: true)
            }
        case(2,indexPath.row):
            if self.editingEnabled{
                cell = tableView.dequeueReusableCell(withIdentifier: "detailTextFieldCell", for: indexPath) as! DetailsTextFieldTableViewCell
                (cell as! DetailsTextFieldTableViewCell).titleLabel.text = "Fahrer \(indexPath.row+1)"
                (cell as! DetailsTextFieldTableViewCell).detailTextField.delegate = self
                (cell as! DetailsTextFieldTableViewCell).detailTextField.tag = 20+indexPath.row
                switch indexPath.row{
                case 0:
                    (cell as! DetailsTextFieldTableViewCell).detailTextField.text = fahrer1
                case 1:
                    (cell as! DetailsTextFieldTableViewCell).detailTextField.text = fahrer2
                case 2:
                    (cell as! DetailsTextFieldTableViewCell).detailTextField.text = fahrer3
                default: break
                }
                cell.selectionStyle = .none
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: "fahrerCell", for: indexPath)
                switch indexPath.row{
                case 0:
                    cell.textLabel?.text = fahrer1
                case 1:
                    cell.textLabel?.text = fahrer2
                case 2:
                    cell.textLabel?.text = fahrer3
                default: break
                }
                cell.selectionStyle = .none
                cell.accessoryType = .none
            }
        default:
            cell = UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.editingEnabled{
            return
        }
        switch(indexPath.section, indexPath.row){
        case(0,0):
            for subview in self.pickerPlaceholderView.subviews{
                subview.removeFromSuperview()
            }
            let picker = UIPickerView(frame: self.pickerPlaceholderView.bounds)
            picker.delegate = self
            picker.dataSource = self
            self.pickerPlaceholderView.addSubview(picker)
            self.mannschaftsPicker = picker
            if self.mannschaft != nil{
                if let index = spielplaene.index(where: {$0.mannschaft == self.mannschaft!}){
                    picker.selectRow(index, inComponent: 0, animated: false)
                }
            }
            showPickerViewWrapper()
            tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        case(0,1):
            let datePicker = UIDatePicker(frame: self.pickerPlaceholderView.bounds)
            datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
            datePicker.datePickerMode = .date
            self.datePicker = datePicker
            for subview in self.pickerPlaceholderView.subviews{
                subview.removeFromSuperview()
            }
            self.pickerPlaceholderView.addSubview(datePicker)
            if self.date != nil{
                datePicker.setDate(self.date!, animated: false)
            }
            showPickerViewWrapper()
            tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        case(0,2):
            /*self.view.bringSubview(toFront: mapWrapperView)
            UIView.animate(withDuration: 0.5, animations: {
                self.mapWrapperView.alpha = 1
            })*/
            //performSegue(withIdentifier: "showMap", sender: nil)
            if self.destinationName != nil{
               searchForPlace()
            }else{
               pickPlace(nil)
            }
            
        default:
            break
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 3{
            return 200
        }
        return tableView.rowHeight
    }
    
    // MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return spielplaene.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return spielplaene[row].mannschaft
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.mannschaft = spielplaene[row].mannschaft
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    // Keyboard Methods
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeFieldFrame = textField.frame
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.isEditing = false
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        switch textField.tag {
        case 20:
            self.fahrer1 = textField.text
        case 21:
            self.fahrer2 = textField.text
        case 22:
            self.fahrer3 = textField.text
        default:
            break
        }
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.tableView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, tableView.contentInset.bottom + keyboardSize!.height, 0.0)
        self.originalInset = self.tableView.contentInset
        self.tableView.contentInset = contentInsets
        self.tableView.scrollIndicatorInsets = contentInsets
        
        self.tapAround = hideKeyboardWhenTappedAround()
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeFieldFrame{
            self.tableView.scrollRectToVisible(activeField, animated: true)
            //self.tableView.setContentOffset(CGPoint(x:0, y:y-30), animated: true)
        }
        
    }
    
    func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        //var info = notification.userInfo!
        dontHideKeyboard(tap: self.tapAround)
        if self.originalInset != nil{
          self.tableView.contentInset = self.originalInset!
        }
        else{
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        self.tableView.scrollIndicatorInsets = self.originalInset!
        self.view.endEditing(true)
        //self.scrollView.isScrollEnabled = false
    }
    
    
    // MARK: Methods
    
    func changeEditView(){
        var indexPaths:[IndexPath] = []
        
        for i in 0...2{
            indexPaths.append(IndexPath(row: i, section: 0))
        }
        self.tableView.reloadRows(at: indexPaths, with: .fade)
        self.tableView.reloadSections(IndexSet(integer: 1), with: .fade)
        self.tableView.reloadSections(IndexSet(integer: 2), with: .fade)
        
    }
    
    func changeBusSwitch(){
        let indexPath1 = IndexPath(row: 1, section: 2)
        let indexPath2 = IndexPath(row: 2, section: 2)
        if self.busSwitch!.isOn{
            self.multiplikator = 1
            self.tableView.deleteRows(at: [indexPath1,indexPath2], with: .bottom)
        }else{
            self.multiplikator = 3
            self.tableView.insertRows(at: [indexPath1,indexPath2], with: .top)
        }
        self.tableView.reloadRows(at: [IndexPath.init(row: 2, section: 1)], with: .none)
    }
    
    func showPickerViewWrapper(){
        if self.pickerWrapperView.frame == self.pickerOrigin{
            UIView.animate(withDuration: 0.3, animations: {
                let f = self.view.bounds
                var offset:CGFloat = 0
                if let o = self.tabBarController?.tabBar.frame.height{
                    offset = o
                }else if let o = self.navigationController?.tabBarController?.tabBar.frame.height{
                    offset = o
                }
                
                self.pickerWrapperView.frame = CGRect(x: 0, y: f.height-(180+offset), width: f.width, height: 180)
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.pickerWrapperView.frame = self.pickerOrigin!
            })

        }
    }
    
    func getFormattedDate(date: Date?)->String?{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        if date != nil{
            let dateString = formatter.string(from: date!)
            return dateString
        }
        return ""
    }

    
    @objc func updateDestination(notification: NSNotification){
        if let userInfo = notification.userInfo{
            /*self.selectedDestination = userInfo["destinationSelected"] as? DestinationDistance
            if let placemark = self.selectedDestination?.mapItem.placemark{
               self.destinationName = "\(placemark.name ?? ""), \(placemark.postalCode ?? ""), \(placemark.locality ?? "")"
            }
            
            if let distance =  self.selectedDestination?.distance{
                self.destinationDistance = distance*2
            }*/
            if let distance = userInfo["distance"] as? Double{
                self.destinationDistance = distance*2
            }
            DispatchQueue.main.async {
                //self.tableView.reloadData()
                let indexSet = IndexSet.init(integer: 1)
                self.tableView.reloadSections(indexSet, with: .none)
            }
        }
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker){
        self.date = sender.date
        let indexPath = IndexPath(row: 1, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }

    @IBAction func cancelButtonClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pickerDoneButtonClicked(_ sender: Any) {
        
        for subview in self.pickerPlaceholderView.subviews{
            if subview === self.datePicker{
                self.date = self.datePicker?.date
                let indexPath = IndexPath(row: 1, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }else if subview === self.mannschaftsPicker{
                self.mannschaft = spielplaene[self.mannschaftsPicker!.selectedRow(inComponent: 0)].mannschaft
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.pickerWrapperView.frame = self.pickerOrigin!
        })
    }
    
    func editButtonPressed(){
        self.editingEnabled = true
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.saveFahrtkosten(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
        
        self.originalBarButton = self.navigationItem.leftBarButtonItem
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelEditing))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        //self.tableView.reloadData()
        changeEditView()
    }
    
    func cancelEditing(){
        self.editingEnabled = false
        let editButton = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(self.editButtonPressed))
        self.navigationItem.rightBarButtonItem = editButton
        
        /*let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelButtonClicked(_:)))
        self.navigationItem.leftBarButtonItem = cancelButton*/
        self.navigationItem.leftBarButtonItem = originalBarButton
        
        //self.tableView.reloadData()
        changeEditView()
    }
    
    func searchForPlace(){
        placesClient.autocompleteQuery(self.destinationName!, bounds: nil, filter: nil, callback: {results, error in
            if error != nil{
                print("Error with results \(error!)")
            }
            if let result = results?.first(where: {$0.placeID != nil}){
                self.placesClient.lookUpPlaceID(result.placeID!, callback: {place, error in
                    self.pickPlace(place?.coordinate)
                })
            }
        })

    }
    
    func pickPlace(_ coord: CLLocationCoordinate2D?) {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        
        var center = CLLocationCoordinate2D(latitude: 49.5615995, longitude: 8.9687293)
        if coord != nil{
            center = coord!
        }else if locationManager.location != nil{
            center = locationManager.location!.coordinate
        }
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                self.destinationName = place.name
                if let addressStr = place.formattedAddress{
                    self.destinationName = addressStr
                }
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "destinationSelected"), object: nil, userInfo: userInfo)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    @IBAction func saveFahrtkosten(_ sender: Any) {
        if fahrtkosten != nil && fahrtkosten?.id != nil{
            /*if var updatedFahrtkosten = Fahrtkosten(datum: self.date, mannschaft: self.mannschaft, ort: self.destinationName, bus: multiplikator == 1, fahrer1: self.fahrer1, fahrer2: self.fahrer2, fahrer3: self.fahrer3, entfernung: self.destinationDistance?.description, kosten: self.kosten.description, gesamtkosten: self.gesamtKosten.description){
                updatedFahrtkosten.id = self.fahrtkosten?.id
            }*/
            self.fahrtkosten?.setDate(self.date!)
            self.fahrtkosten?.mannschaft = self.mannschaft
            self.fahrtkosten?.ort = self.destinationName
            if multiplikator == 1{
                self.fahrtkosten?.bus = "Ja"
                self.fahrtkosten?.fahrer2 = nil
                self.fahrtkosten?.fahrer3 = nil
            }else{
                self.fahrtkosten?.bus = "Nein"
                self.fahrtkosten?.fahrer2 = self.fahrer2
                self.fahrtkosten?.fahrer3 = self.fahrer3
            }
            self.fahrtkosten?.fahrer = self.fahrer1
            if self.destinationDistance != nil{
                self.fahrtkosten?.entfernung = (self.destinationDistance!).roundTo(places: 2).description
            }
            self.fahrtkosten?.kosten = self.kosten.description
            self.fahrtkosten?.gesamtkosten = self.gesamtKosten.description
            
            self.fahrtkosten?.update()
            //WebService.shared.updateFahrtkosten(self.fahrtkosten!)
            self.cancelEditing()
            _ = navigationController?.popViewController(animated: true)
            
        }else if let entfernung = self.destinationDistance, let fahrtkosten = Fahrtkosten(datum: self.date, mannschaft: self.mannschaft, ort: self.destinationName, bus: multiplikator == 1, fahrer1: self.fahrer1, fahrer2: self.fahrer2, fahrer3: self.fahrer3, entfernung: (entfernung).roundTo(places:2).description, kosten: self.kosten.description, gesamtkosten: self.gesamtKosten.description){
            //WebService.shared.createFahrtkosten(fahrtkosten)
            fahrtkosten.create()
            unfilteredFahrtkosten.append(fahrtkosten)
            
        }
        self.dismiss(animated: true, completion: nil)
        //_ = navigationController?.popViewController(animated: true)
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap"{
            if let destination = segue.destination as? MapViewController{
                destination.selectedDestination = self.selectedDestination
                destination.destinationName = self.destinationName
            }
        }
    }
    

}
