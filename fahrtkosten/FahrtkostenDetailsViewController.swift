//
//  FahrtkostenDetailsViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 07.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MapKit

class FahrtkostenDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Attributes
    @IBOutlet weak var detailsTableView: UITableView!
    
    var fahrtkosten: Fahrtkosten?
    
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    let regionRadius: CLLocationDistance = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsTableView.delegate = self
        detailsTableView.dataSource = self
        
        self.title = fahrtkosten?.ort
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }else{
            var count = 1
            if let fahrer2 = fahrtkosten?.fahrer2{
                if !fahrer2.isEmpty{
                    count += 1
                }
            }
            if let fahrer3 = fahrtkosten?.fahrer3{
                if !fahrer3.isEmpty{
                    count += 1
                }
            }
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1{
            return "Fahrer"
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0.1
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0{
            return 300
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        switch (indexPath.section, indexPath.row){
        case (0,0):
            cell = tableView.dequeueReusableCell(withIdentifier: "mapsCell", for: indexPath) as! MapsTableViewCell
            (cell as! MapsTableViewCell).titleLabel.text = fahrtkosten?.mannschaft
            (cell as! MapsTableViewCell).subtitleLabel.text = fahrtkosten?.getFormattedDate()
            if let ort = fahrtkosten?.ort{
                setMapview(str: ort, cell: cell as! MapsTableViewCell)
            }
            
        case (0,1):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Entfernung"
            cell.detailTextLabel?.text = fahrtkosten?.entfernung?.description
        case (0,2):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Kosten"
            cell.detailTextLabel?.text = fahrtkosten?.kosten?.description
        case (0,3):
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
            cell.textLabel?.text = "Gesamtkosten"
            cell.detailTextLabel?.text = fahrtkosten?.gesamtkosten?.description
        case (0,4):
            cell = tableView.dequeueReusableCell(withIdentifier: "busCell", for: indexPath) as! busTableViewCell
            if fahrtkosten?.bus?.lowercased() == "ja"{
                (cell as! busTableViewCell).busSwitch.setOn(true, animated: false)
            }else{
                (cell as! busTableViewCell).busSwitch.setOn(false, animated: false)
            }

        case (1,0):
            cell = tableView.dequeueReusableCell(withIdentifier: "fahrerCell", for: indexPath)
            cell.textLabel?.text = fahrtkosten?.fahrer
        case (1,1):
            cell = tableView.dequeueReusableCell(withIdentifier: "fahrerCell", for: indexPath)
            cell.textLabel?.text = fahrtkosten?.fahrer2
        case (1,2):
            cell = tableView.dequeueReusableCell(withIdentifier: "fahrerCell", for: indexPath)
            cell.textLabel?.text = fahrtkosten?.fahrer3
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath)
        }
        return cell
    }
    
    // MARK: Methods
    
    func setMapview(str: String, cell: MapsTableViewCell){
        // set initial location in Honolulu
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = str
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                return
            }
            //3
            self.pointAnnotation = MKPointAnnotation()
            self.pointAnnotation.title = str
            self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
            
            
            self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
            cell.ortMapView.centerCoordinate = self.pointAnnotation.coordinate
            cell.ortMapView.addAnnotation(self.pinAnnotationView.annotation!)
            
            let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
            let span = MKCoordinateSpanMake(0.2, 0.2)
            let region = MKCoordinateRegionMake(coordinate, span)
            cell.ortMapView.setRegion(region, animated: true)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
