//
//  mapOnlyTableViewCell.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 23.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MapKit
import NotificationCenter

class mapOnlyTableViewCell: UITableViewCell, MKMapViewDelegate {
    
    // MARK: Attributes
    @IBOutlet weak var mapView: MKMapView!
    
    let initialLocation = CLLocation(latitude: 49.5615995, longitude: 8.9687293)
    let regionRadius: CLLocationDistance = 1000
    
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MapView Delegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseId = "pin"
        if annotation === self.pointAnnotation{
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
            
            if pinView == nil {
                //println("Pinview was nil")
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView!.canShowCallout = false
                pinView!.animatesDrop = true
            }
            
            
            return pinView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    // MARK: Methods
    
    func setLocation(_ locationName:String){
        
        self.mapView.removeOverlays(self.mapView.overlays)
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = locationName
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                if let vc = UIApplication.shared.delegate?.window??.rootViewController{
                    vc.present(alertController, animated: true, completion: nil)
                }
                return
            }
            self.pointAnnotation = MKPointAnnotation()
            self.pointAnnotation.title = locationName
            self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
            
            
            self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
            self.mapView.centerCoordinate = self.pointAnnotation.coordinate
            self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
            
            let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
            let span = MKCoordinateSpanMake(0.2, 0.2)
            let region = MKCoordinateRegionMake(coordinate, span)
            self.mapView.setRegion(region, animated: true)
            self.drawRoute(destinationLocation: coordinate)
        }
    }
    
    func drawRoute(destinationLocation: CLLocationCoordinate2D){
        let sourcePlacemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.initialLocation.coordinate.latitude, longitude: self.initialLocation.coordinate.longitude), addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        // 5.
        //let sourceAnnotation = MKPointAnnotation()
        //sourceAnnotation.title = "Times Square"
        
        //if let location = sourcePlacemark.location {
            //sourceAnnotation.coordinate = location.coordinate
        //}
        
        
        //let destinationAnnotation = MKPointAnnotation()
        //destinationAnnotation.title = "Empire State Building"
        
        //if let location = destinationPlacemark.location {
            //destinationAnnotation.coordinate = location.coordinate
        //}
        
        // 6.
        //self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        // 7.
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            var region = MKCoordinateRegionForMapRect(rect)
            region.span = MKCoordinateSpanMake(0.8, 0.8)
            self.mapView.setRegion(region, animated: true)
            let userInfo = ["distance": route.distance/1000]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "destinationSelected"), object: nil, userInfo: userInfo)
        }
    }

}
