//
//  SpielPlanViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 10.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MGSwipeTableCell



class SpielPlanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: Attributes
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerWrapperView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var spiele = [Spiel]()
    //var spielplaene = [Spielplan]()
    var refreshControl: UIRefreshControl?
    
    var selectMatch = false
    var fromPush = false
    
    var pickerOrigin: CGRect?
    
    override func viewDidLoad() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.loadData), for: .valueChanged)
        tableView.refreshControl = refreshControl
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.pickerOrigin = pickerWrapperView.frame
        if !fromPush{
            loadData()
        }
        //loadData()
        
        //let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.filtetButtonClicked(_:)))
        //self.navigationController?.navigationBar.addGestureRecognizer(tapRecognizer)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spiele.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "spielCell", for: indexPath) as! SpielTableViewCell
        let spiel = spiele[indexPath.row]
        
        if let datum = spiel.datum, let uhrzeit = spiel.zeit{
           cell.dateLabel.text = datum+", "+uhrzeit
        }
        if var heim = spiel.heim, var gast = spiel.gast{
            if spiel.isHeimspiel(){
                heim = "TVB"
            }else{
                gast = "TVB"
            }
            
            cell.matchLabel.text = heim+" - "+gast
        }
        cell.scoreLabel.text = spiel.tore
        if spiel.ort != nil{
            cell.ortButton.setTitle(spiel.ort, for: .normal)
            cell.ortButton.isHidden = false
        }else{
          cell.ortButton.isHidden = true
        }
        
        
        let createButton = MGSwipeButton(title: " ", icon: #imageLiteral(resourceName: "plus"), backgroundColor: UIColor.darkGray, callback: { _ in
            self.performSegue(withIdentifier: "createFromMatch", sender: cell)
            return true
        })
        createButton.buttonWidth = 70
        
        if !spiel.isHeimspiel(){
            cell.rightButtons = [createButton]
        }else{
            cell.rightButtons = []
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectMatch{
            let cell = tableView.cellForRow(at: indexPath)
            self.performSegue(withIdentifier: "selectedFromMatch", sender: cell)
        }
    }
    
    // MARK: UIPicker
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return spielplaene.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return spielplaene[row].mannschaft
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.spiele = spielplaene[row].spiele
        DispatchQueue.main.async {
            self.navigationItem.title = spielplaene[row].mannschaft
            self.tableView.reloadData()
        }
    }
    
    // MARK: Methods
    
    func dimissView(){
        self.dismiss(animated: true, completion: nil)
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func loadData(){
        self.refreshControl?.endRefreshing()
        showActivityIndicatory()
        WebService.shared.getSpiele(completion: {plaene in
            spielplaene = plaene
            
            if spielplaene.count > 0{
                self.spiele = spielplaene[0].spiele
            }
            
            DispatchQueue.main.async {
                self.hideActivityIndicatory()
                self.tableView.reloadData()
                self.pickerView.reloadAllComponents()
                //self.title = spielplaene[0].mannschaft
                self.navigationItem.title = spielplaene[0].mannschaft
            }
        })
    }
    
    
    // MARK: Action
    @IBAction func pickerDoneButtonClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.pickerWrapperView.frame = self.pickerOrigin!
        })
    }
    @IBAction func filtetButtonClicked(_ sender: Any) {
        if self.pickerWrapperView.frame == self.pickerOrigin{
            UIView.animate(withDuration: 0.3, animations: {
                let f = self.view.bounds
                var offset:CGFloat = 0
                if let o = self.tabBarController?.tabBar.frame.height{
                    offset = o
                }else if let o = self.navigationController?.tabBarController?.tabBar.frame.height{
                    offset = o
                }
                
                self.pickerWrapperView.frame = CGRect(x: 0, y: f.height-(180+offset), width: f.width, height: 180)
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.pickerWrapperView.frame = self.pickerOrigin!
            })
            
        }
    }
    
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "createFromMatch"{
            if let navVC = segue.destination as? UINavigationController, let destination = navVC.topViewController as? CreateFahrtkostenViewController{
                if let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell){
                    let spiel = spiele[indexPath.row]
                    let fahrtkosten = Fahrtkosten(datum: spiel.getDate(), mannschaft: self.navigationItem.title, ort: spiel.ort, bus: true, fahrer1: nil, fahrer2: nil, fahrer3: nil, entfernung: nil, kosten: nil, gesamtkosten: nil)
                    destination.fahrtkosten = fahrtkosten
                }
            }
        }else if segue.identifier == "selectedFromMatch"{
            if let destination = segue.destination as? CreateFahrtkostenViewController, let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell){
                let spiel = spiele[indexPath.row]
                let fahrtkosten = Fahrtkosten(datum: spiel.getDate(), mannschaft: self.navigationItem.title, ort: spiel.ort, bus: true, fahrer1: nil, fahrer2: nil, fahrer3: nil, entfernung: nil, kosten: nil, gesamtkosten: nil)
                destination.fahrtkosten = fahrtkosten
            }
        }
    }
 

}
