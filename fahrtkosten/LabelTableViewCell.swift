//
//  LabelTableViewCell.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 26.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
