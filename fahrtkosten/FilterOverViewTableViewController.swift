//
//  FilterOverViewTableViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 24.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import NotificationCenter

class FilterOverViewTableViewController: UITableViewController {
    
    //MARK: Attributes
    @IBOutlet weak var mannschaftCell: UITableViewCell!
    @IBOutlet weak var busCell: UITableViewCell!
    @IBOutlet weak var vonCell: UITableViewCell!
    @IBOutlet weak var bisCell: UITableViewCell!
    @IBOutlet weak var sortByCell: UITableViewCell!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        updateDetailLabels()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDetailLabels), name: Notification.Name(rawValue:"filtersChanged"), object: nil)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    /*override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: Methods
    
    func updateDetailLabels(){
        self.mannschaftCell.detailTextLabel?.text = getLabelStr(.mannschaft)
        if mannschaftCell.detailTextLabel?.text == nil || mannschaftCell.detailTextLabel!.text!.isEmpty{
            mannschaftCell.detailTextLabel?.text = "Alle"
        }
        self.busCell.detailTextLabel?.text = getLabelStr(.hasBus)
        if busCell.detailTextLabel?.text == nil || busCell.detailTextLabel!.text!.isEmpty{
            busCell.detailTextLabel?.text = "Alle"
        }
        self.vonCell.detailTextLabel?.text = myFilters.first(where: {$0.filterType == .dateFrom})?.getFormattedDate()
        if vonCell.detailTextLabel?.text == nil || vonCell.detailTextLabel!.text!.isEmpty{
            vonCell.detailTextLabel?.text = "Alle"
        }
        self.bisCell.detailTextLabel?.text = myFilters.first(where: {$0.filterType == .dateTo})?.getFormattedDate()
        if bisCell.detailTextLabel?.text == nil || bisCell.detailTextLabel!.text!.isEmpty{
            bisCell.detailTextLabel?.text = "Alle"
        }
        self.sortByCell.detailTextLabel?.text = myFilters.first(where: {$0.filterType == .sortBy})?.searchString
        if sortByCell.detailTextLabel?.text == nil || sortByCell.detailTextLabel!.text!.isEmpty{
            sortByCell.detailTextLabel?.text = "Alle"
        }
    }
    
    func getLabelStr(_ filterType: FilterType)->String{
        var filterStr = ""
        let activeFilters = myFilters.filter({$0.filterType == filterType})
        for (index, filter) in activeFilters.enumerated(){
            if filter.searchString != nil{
               filterStr += filter.searchString!
            }
            if index < activeFilters.count-1{
                filterStr += ", "
            }
        }
        
        if filterStr.isEmpty{
            filterStr = "Alle"
        }
        return filterStr
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let cell = sender as? UITableViewCell{
            segue.destination.navigationItem.title = cell.textLabel?.text
        }
        if segue.identifier != nil{
            switch segue.identifier!{
                case "mannschaftFilterDetail":
                    if let destination = segue.destination as? FilterDetailTableViewController{
                        destination.filterType = .mannschaft
                    }
                case "busFilterDetail":
                    if let destination = segue.destination as? FilterDetailTableViewController{
                        destination.filterType = .hasBus
                    }
                case "fromFilterDetail":
                    if let destination = segue.destination as? FilterSelectDateViewController{
                        destination.filterType = .dateFrom
                    }
                case "toFilterDetail":
                    if let destination = segue.destination as? FilterSelectDateViewController{
                        destination.filterType = .dateTo
                    }
                case "sortyByFilterDetail":
                    if let destination = segue.destination as? FilterDetailTableViewController{
                        destination.filterType = .sortBy
                    }
            default:
                break
            }
        }
    }
 

}
