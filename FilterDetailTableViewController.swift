//
//  FilterDetailTableViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 24.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit

class FilterDetailTableViewController: UITableViewController {
    
    var filterType: FilterType?

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.filterType == .sortBy{
            tableView.allowsMultipleSelection = false
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.filterType == .hasBus{
            return 2
        }else if filterType == .mannschaft{
            return spielplaene.count
        }else if filterType == .sortBy{
           return SortBy.count
        }else{
            return 0
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterDetailCell", for: indexPath)
        if self.filterType == .mannschaft{
           cell.textLabel?.text = spielplaene[indexPath.row].mannschaft
        }else if filterType == .hasBus{
            if indexPath.row == 0{
                cell.textLabel?.text = "Ja"
            }else if indexPath.row == 1{
                cell.textLabel?.text = "Nein"
            }
        }else if filterType == .sortBy{
            cell.textLabel?.text = SortBy.fromHashValue(indexPath.row)?.rawValue
        }
        
        if myFilters.contains(where: {$0.searchString == cell.textLabel?.text}){
            cell.accessoryType = .checkmark
            cell.setSelected(true, animated: true)
            cell.isSelected = true
        }else{
            cell.accessoryType = .none
            cell.setSelected(false, animated: true)
            cell.isSelected = false
        }
        
        cell.tintColor = UIColor.red
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if let cell = self.tableView.cellForRow(at: indexPath), let searchStr = cell.textLabel?.text{
            if !myFilters.contains(where: {$0.searchString == searchStr}) && !cell.isSelected{
                if let newFilter = FilterElement(filterType: self.filterType!, searchString: searchStr, searchDate: nil){
                    myFilters.append(newFilter)
                }
                cell.accessoryType = .checkmark
            }else if let index = myFilters.index(where: {$0.searchString == searchStr}){
                cell.accessoryType = .none
                cell.isSelected = false
                cell.setSelected(false, animated: true)
                myFilters.remove(at: index)
            }
        }
        if filterType == .sortBy{
            selectSortBy(indexPath)
        }
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        if let cell = self.tableView.cellForRow(at: indexPath), let searchStr = cell.textLabel?.text, let index = myFilters.index(where: {$0.searchString == searchStr}){
            myFilters.remove(at: index)
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
        return indexPath
    }
    
    func selectSortBy(_ indexPath: IndexPath){
        if let index = myFilters.index(where: {$0.filterType == .sortBy}){
            myFilters.remove(at: index)
        }
        for i in 0..<SortBy.count{
            let ip = IndexPath(row: i, section: 0)
            tableView.deselectRow(at: ip, animated: false)
            let cell = tableView.cellForRow(at: ip)
            cell?.accessoryType = .none
            cell?.isSelected = false
            cell?.setSelected(false, animated: true)
        }
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        cell?.isSelected = true
        cell?.setSelected(true, animated: true)
        print(cell?.textLabel?.text)
        if !myFilters.contains(where: {$0.filterType == filterType}){
            if let searchStr = cell?.textLabel?.text, let newFilter = FilterElement(filterType: self.filterType!, searchString: searchStr, searchDate: nil){
                myFilters.append(newFilter)
            }
        }
        
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
