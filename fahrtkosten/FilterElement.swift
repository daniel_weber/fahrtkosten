//
//  FilterElement.swift
//  Pods
//
//  Created by Daniel Weber on 24.06.17.
//
//

import Foundation
import NotificationCenter

enum FilterType: String{
    case mannschaft = "Mannschaft"
    case hasBus = "Bus"
    case dateFrom = "Datum von"
    case dateTo = "Datum bis"
    case sortBy = "Sortieren nach"
}

enum SortBy:String{
    case dateAsc = "Datum älteste zuerst"
    case dateDesc = "Datum neuste zuerst"
    case distanceAsc = "Entfernung aufsteigend"
    case distanceDesc = "Entfernung absteigend"
    case costsAsc = "Gesamtkosten aufsteigend"
    case costsDesc = "Gesamtkosten absteigend"
    static var count: Int { return SortBy.costsDesc.hashValue + 1}
    static func fromHashValue(_ hashValue: Int) -> SortBy? {
        switch hashValue{
        case 0:
            return .dateAsc
        case 1:
            return .dateDesc
        case 2:
            return .distanceAsc
        case 3:
            return .distanceDesc
        case 4:
            return .costsAsc
        case 5:
            return .costsDesc
        default:
            return nil
        }
    }
    
}

var myFilters = [FilterElement](){
    didSet{
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "filtersChanged"), object: nil)
    }
}

class FilterElement{
    
    let filterType: FilterType
    let searchString: String?
    var searchDate: Date?
    
    init?(filterType: FilterType, searchString: String?, searchDate: Date?){
        if (searchString == nil && (filterType == .mannschaft || filterType == .sortBy)) || (searchDate == nil && filterType != .sortBy && filterType != .mannschaft && filterType != .hasBus){
            print("Required values are nil: \(searchString)")
            return nil
        }
        
        self.filterType = filterType
        self.searchDate = searchDate
        if searchDate != nil{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.searchString = formatter.string(from: searchDate!)
        }else{
            self.searchString = searchString
        }
    }
    
    func getFormattedDate()->String?{
        return getFormattedDate(format: "dd.MM.yyyy")
    }
    
    func getFormattedDate(format: String)->String?{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if self.searchDate != nil{
            let dateString = formatter.string(from: self.searchDate!)
            return dateString
        }
        return "Alle"
    }
    
}
