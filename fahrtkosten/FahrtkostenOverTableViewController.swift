//
//  FahrtkostenOverTableViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 07.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import MGSwipeTableCell
import NotificationCenter
import SACountingLabel

var unfilteredFahrtkosten = [Fahrtkosten]()

class FahrtkostenOverTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    var fahrtkostenList = [Fahrtkosten]()
    var refreshControl: UIRefreshControl?
    
    var lastKnownOffset:CGFloat = 0
    var offestToUse: CGFloat = 0
    
    var previousScrollViewYOffset:CGFloat = 0
    
    var scrollDirection = "down"
    var fullyHidden = false
    var fullyShown = true
    var sumOld: Float?
    var kmSumOld: Float?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fahrtkostenOverView: UIView!
    @IBOutlet weak var fahrtkostenGesamtLabel: SACountingLabel!
    @IBOutlet weak var kmGesamtLabel: SACountingLabel!
    
    @IBOutlet weak var pdfIcon: UIBarButtonItem!
    @IBOutlet weak var filterBarButton: UIBarButtonItem!
    @IBOutlet weak var creatFahrtkostenButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.loadData), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        tableView.delegate = self
        tableView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkRole), name: Notification.Name(rawValue: "updatedRoles"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.filtersChanged), name: Notification.Name(rawValue:"filtersChanged"), object: nil)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = fahrtkostenOverView.bounds
        blurEffectView.alpha = 0.8
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        fahrtkostenOverView.insertSubview(blurEffectView, at: 0)
        fahrtkostenOverView.layer.shadowColor = UIColor.black.cgColor
        fahrtkostenOverView.layer.shadowOpacity = 0.5
        fahrtkostenOverView.layer.shadowOffset = CGSize.zero
        fahrtkostenOverView.layer.shadowRadius = 5
        fahrtkostenOverView.alpha = 0.9
        
        tableView.contentInset = UIEdgeInsets(top: 70, left: 0, bottom: 0, right: 0)
        
        self.loadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.creatFahrtkostenButton.isEnabled = currentUser != nil && currentUser!.role != .readOnly
        if !WebService.shared.isLoggedIn(){
            self.showLoginSheet()
        }else{
            self.filterList()
        }
        filtersChanged()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return fahrtkostenList.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fahrtkostenCell", for: indexPath) as! MGSwipeTableCell
        if let ort = fahrtkostenList[indexPath.row].ort{
            cell.textLabel?.text = ort
        }
        
        if let mannschaft = fahrtkostenList[indexPath.row].mannschaft, let datum = fahrtkostenList[indexPath.row].getFormattedDate(){
          cell.detailTextLabel?.text = datum+" - "+mannschaft
        }
        
        let delete = MGSwipeButton(title: " ", backgroundColor: UIColor.red, callback: {_ in
            self.showDeleteAlert(cell: cell)
            return false
        })
        delete.setImage(#imageLiteral(resourceName: "trash"), for: .normal)
        delete.setImage(#imageLiteral(resourceName: "trashFilled"), for: .selected)
        delete.setImage(#imageLiteral(resourceName: "trashFilled"), for: .highlighted)
        delete.buttonWidth = 70
        
        let editButton = MGSwipeButton(title: " ", backgroundColor: UIColor.darkGray, callback: {_ in
            self.performSegue(withIdentifier: "editFahrtkosten", sender: cell)
            return true
        })
        editButton.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
        editButton.setImage(#imageLiteral(resourceName: "editFilled"), for: .selected)
        editButton.setImage(#imageLiteral(resourceName: "editFilled"), for: .highlighted)
        editButton.buttonWidth = 70
        
        let editIndicator = UIView(frame: CGRect(x: 5, y: 2, width: 2, height: cell.frame.height-4))
        
        if currentUser?.role == .admin || fahrtkostenList[indexPath.row].email == currentUser?.email{
            cell.rightButtons = [delete,editButton]
            editIndicator.backgroundColor = UIColor.red
        }else{
            cell.rightButtons = []
            editIndicator.backgroundColor = UIColor.lightGray
        }
        
        cell.contentView.addSubview(editIndicator)

        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var frame: CGRect = self.fahrtkostenOverView.frame
        //let size: CGFloat = frame.size.height - 21
        //let framePercentageHidden: CGFloat? = (20 - frame.origin.y) / (frame.size.height - 1)
        
        let size: CGFloat = frame.size.height
        let framePercentageHidden: CGFloat = (0 - frame.origin.y) / (frame.size.height - 1)
        
        let scrollOffset: CGFloat = scrollView.contentOffset.y
        let scrollDiff: CGFloat = scrollOffset - previousScrollViewYOffset
        let scrollHeight: CGFloat = scrollView.frame.size.height
        let scrollContentSizeHeight: CGFloat = scrollView.contentSize.height + scrollView.contentInset.bottom
        /*if scrollOffset <= -scrollView.contentInset.top {
            frame.origin.y = 0
        }*/
        if scrollOffset <= tableView.rowHeight - 70 {
            frame.origin.y = 0
        }
        else if (scrollOffset + scrollHeight) >= scrollContentSizeHeight {
            frame.origin.y = -size
        }
        else {
            frame.origin.y = min(0, max(-size, frame.origin.y - scrollDiff))
        }
        
        self.fahrtkostenOverView.frame = frame
        updateBarButtonItems((1 - framePercentageHidden))
        previousScrollViewYOffset = scrollOffset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        stoppedScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            stoppedScrolling()
        }
    }
    

    func stoppedScrolling() {
        let frame: CGRect = self.fahrtkostenOverView.frame
        if frame.origin.y < 0 {
            animateNavBar(to: -(frame.size.height - 0))
        }
    }
    
    func updateBarButtonItems(_ alpha: CGFloat) {
        UIView.animate(withDuration: 0.2, animations: {
            self.kmGesamtLabel.alpha = alpha
            self.fahrtkostenGesamtLabel.alpha = alpha
        })
    }
    
    func animateNavBar(to y: CGFloat) {
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
            var frame: CGRect = self.fahrtkostenOverView.frame
            let alpha: CGFloat = (frame.origin.y >= y ? 0 : 1)
            frame.origin.y = y
            self.fahrtkostenOverView.frame = frame
            self.updateBarButtonItems(alpha)
        })
    }
    
    
    

    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if currentUser?.role == .admin{
            return true
        }
        if fahrtkostenList[indexPath.row].autor_id == currentUser?.id{
            return true
        }
        return false
    }
    
    /*func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .normal, title: "Löschen", handler: {(action, indexPath) in
            
        })
        delete.backgroundColor = UIColor.red
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit", handler: {(action, indexPath) in
            
        })
        edit.backgroundColor = UIColor.darkGray
        if currentUser?.role == .admin || fahrtkostenList[indexPath.row].autor_id == currentUser?.id{
            return [delete,edit]
        }
        return nil
    }*/

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: Methods
    
    func loadData(){
        self.refreshControl?.endRefreshing()
        self.tableView.refreshControl?.endRefreshing()
        self.showActivityIndicatory()
        WebService.shared.getFahrtkosten(completion: {(list)->Void in
            //self.fahrtkostenList = list
            unfilteredFahrtkosten = list
            DispatchQueue.main.async {
                //self.tableView.reloadData()
                self.filterList()
                self.getOverall()
                self.hideActivityIndicatory()
            }
        })
    }
    
    func getOverall(){
        var sum:Double = 0
        var kmSum:Double = 0
        for kosten in self.fahrtkostenList{
            if let kStr = kosten.gesamtkosten, let k = Double(kStr){
                sum += k
            }
            if let kmStr = kosten.entfernung, let km = Double(kmStr){
                kmSum += km
            }
        }
        self.animateNavBar(to: 0)
        self.updateBarButtonItems(1)
        previousScrollViewYOffset = tableView.contentOffset.y
        if self.sumOld != nil{
            self.fahrtkostenGesamtLabel.countFrom(fromValue: sumOld!, to: Float(sum), withDuration: 1, andAnimationType: .EaseInOut, andCountingType: .Custom)
            self.fahrtkostenGesamtLabel.format = "%.2f €"
        }else{
           self.fahrtkostenGesamtLabel.text = sum.toEuro()
        }
        if self.kmSumOld != nil{
            self.kmGesamtLabel.countFrom(fromValue: kmSumOld!, to: Float(kmSum), withDuration: 1, andAnimationType: .EaseInOut, andCountingType: .Custom)
            self.kmGesamtLabel.format = "%.2f km"
        }else{
            self.kmGesamtLabel.text = kmSum.description+" km"
        }
        
        sumOld = Float(sum)
        kmSumOld = Float(kmSum)
    }
    
    func showLoginSheet(){
        let loginSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        //loginSheet.view.tintColor = UIColor(rgb: 0xeb3c96)
        let loginAction = UIAlertAction(title: "Login", style: .default, handler: {_ in self.loginButtonClicked()})
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        loginAction.setValue(UIImage(named:"facebookLogo"), forKey: "image")
        loginSheet.addAction(loginAction)
        loginSheet.addAction(cancelAction)
        self.present(loginSheet, animated: true, completion: nil)
    }
    
    func checkRole(){
        self.creatFahrtkostenButton.isEnabled = currentUser != nil && currentUser!.role != .readOnly
        tableView.reloadData()
    }

    
    func showLogOutSheet(){
        let loginSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        //loginSheet.view.tintColor = UIColor(rgb: 0xeb3c96)
        let loginAction = UIAlertAction(title: "Logout", style: .default, handler: {_ in self.logoutButtonClicked()})
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        loginAction.setValue(UIImage(named:"facebookLogo"), forKey: "image")
        loginSheet.addAction(loginAction)
        loginSheet.addAction(cancelAction)
        self.present(loginSheet, animated: true, completion: nil)
    }
    
    func showDeleteAlert(cell: MGSwipeTableCell){
        if let indexPath = tableView.indexPath(for: cell){
            let alert = UIAlertController(title: "Löschen", message: "Eintrag wirklich löschen?", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: {_ in
                cell.hideSwipe(animated: true)
            })
            let ok = UIAlertAction(title: "Löschen", style: .destructive, handler: {_ in self.delete(indexPath: indexPath)})
            alert.addAction(ok)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func delete(indexPath: IndexPath){
        fahrtkostenList[indexPath.row].delete()
        //WebService.shared.deleteFahrtkosten(fahrtkostenList[row])
        
        if let i = unfilteredFahrtkosten.index(where: {$0.id == fahrtkostenList[indexPath.row].id}){
            unfilteredFahrtkosten.remove(at: i)
        }
        fahrtkostenList.remove(at: indexPath.row)
        let cell = self.tableView.cellForRow(at: indexPath) as! MGSwipeTableCell
        self.getOverall()
        cell.hideSwipe(animated: true, completion: {_ in
            self.tableView.deleteRows(at: [indexPath], with: .right)
        })
    }
    
    // Filter
    
    func filterList(){
        var list = unfilteredFahrtkosten
        list = filterForMannschaft(listToFilter: list)
        list = filterForHasBus(listToFilter: list)
        list = filterForFromDate(listToFilter: list)
        list = filterForToDate(listToFilter: list)
        self.fahrtkostenList = list
        sortBy()
        self.tableView.reloadData()
        getOverall()
        
    }
    
    func filterForMannschaft(listToFilter: [Fahrtkosten])->[Fahrtkosten]{
        let filters:[FilterElement] = myFilters.filter({filter in filter.filterType == FilterType.mannschaft})
        if filters.count == 0{
            return listToFilter
        }
        let filteredList = listToFilter.filter({fahrtkosten in filters.contains(where: {singleFilter in singleFilter.searchString!.lowercased() == fahrtkosten.mannschaft?.lowercased()})})
        
        return filteredList
        
    }
    
    
    
    func filterForHasBus(listToFilter: [Fahrtkosten])->[Fahrtkosten]{
        let filters:[FilterElement] = myFilters.filter({filter in filter.filterType == FilterType.hasBus})
        if filters.count == 0{
            return listToFilter
        }
        let filteredList = listToFilter.filter({fahrtkosten in
            filters.contains(where: {singleFilter in singleFilter.searchString!.lowercased() == fahrtkosten.bus?.lowercased()})})
        
        return filteredList
        
    }
    
    func filterForFromDate(listToFilter: [Fahrtkosten])->[Fahrtkosten]{
        let filters:[FilterElement] = myFilters.filter({filter in filter.filterType == FilterType.dateFrom})
        if filters.count == 0{
            return listToFilter
        }
        let filteredList = listToFilter.filter({fahrtkosten in
            filters.contains(where: {singleFilter in singleFilter.searchDate! < fahrtkosten.getDate()!})})
        
        return filteredList
        
    }
    
    func filterForToDate(listToFilter: [Fahrtkosten])->[Fahrtkosten]{
        let filters:[FilterElement] = myFilters.filter({filter in filter.filterType == FilterType.dateTo})
        if filters.count == 0{
            return listToFilter
        }
        let filteredList = listToFilter.filter({fahrtkosten in
            filters.contains(where: {singleFilter in singleFilter.searchDate! > fahrtkosten.getDate()!})})
        
        return filteredList
        
    }
    
    func sortBy(){
        self.fahrtkostenList.sort(by: {a, b in
            if let filter = myFilters.first(where: {$0.filterType == .sortBy}), let sortBy = SortBy(rawValue: filter.searchString!){
                switch sortBy{
                case .costsAsc:
                   return  a.gesamtkosten! < b.gesamtkosten!
                case .costsDesc:
                    return a.gesamtkosten! > b.gesamtkosten!
                case .dateAsc:
                    return a.getDate()! < b.getDate()!
                case .dateDesc:
                    return a.getDate()! > b.getDate()!
                case .distanceAsc:
                    return a.entfernung! < b.entfernung!
                case .distanceDesc:
                    return a.entfernung! > b.entfernung!
                }
            }
            return a.id!<b.id!
        })
    }
    
    func filtersChanged(){
        if myFilters.count == 0{
            self.filterBarButton.image = #imageLiteral(resourceName: "sorting")
        }else{
            self.filterBarButton.image = #imageLiteral(resourceName: "sortingFilled")
        }
    }
    
    // MARK: Actions
    
    // Once the button is clicked, show the login dialog
    
    @IBAction func showCreateActionSheet(_ sender: Any){
        let createSheet = UIAlertController(title: "Fahrtkosten Eintragen", message: nil, preferredStyle: .actionSheet)
        let fromBlueprint = UIAlertAction(title: "Spiel wählen", style: .default, handler: {_ in self.performSegue(withIdentifier: "selectMatch", sender: nil)})
        let fromScratch = UIAlertAction(title: "Ohne Vorlage", style: .default, handler: {_ in self.performSegue(withIdentifier: "createFahrtkosten", sender: nil)})
        let cancel = UIAlertAction(title: "Abbrechen", style: .destructive, handler: nil)
        
        createSheet.addAction(fromBlueprint)
        createSheet.addAction(fromScratch)
        createSheet.addAction(cancel)
        self.present(createSheet, animated: true, completion: nil)
        
    }
    
    func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                WebService.shared.setAccessToken(token: accessToken)
                print("Logged in!")
                print(grantedPermissions)
                print(declinedPermissions)
            }
        }
    }
    
    
    @IBAction func logoutClicked(_ sender: Any) {
        showLogOutSheet()
    }
    func logoutButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logOut()
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(self.showLoginSheet))
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editFahrtkosten"{
            if let navigation = segue.destination as? UINavigationController, let destination = navigation.topViewController as? CreateFahrtkostenViewController, let indexPath = tableView.indexPath(for: sender as! UITableViewCell){
                destination.fahrtkosten = fahrtkostenList[indexPath.row]
            }
        }else if segue.identifier == "showDetailNew"{
            if let destination = segue.destination as? CreateFahrtkostenViewController, let indexPath = tableView.indexPath(for: sender as! UITableViewCell){
                destination.fahrtkosten = fahrtkostenList[indexPath.row]
                destination.editingEnabled = false
            }
        }else if segue.identifier == "selectMatch"{
            if let destination = segue.destination as? SpielPlanViewController{
                destination.selectMatch = true
            }
        }
    }
 

}
