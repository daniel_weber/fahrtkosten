//
//  GeoLocationHelper.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 30.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class GeoLocationHelper: NSObject, CLLocationManagerDelegate{
    
    // MARK: Attributes
    
    //var geotifications = [Geotification]()
    
    var matchesMonitored = [Spiel]()
    var matchesVisited = [Spiel]()
    
    let locationManager = CLLocationManager()
    
    static public let shared = GeoLocationHelper()
    
    override init(){
        super.init()
        self.locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    func region(location: CLLocationCoordinate2D, radius:CLLocationDistance?, identifier: String) -> CLCircularRegion {
        
        let r: CLLocationDistance
        if radius != nil{
            r = radius!
        }else{
            r = 100
        }
        
        // 1
        let region = CLCircularRegion(center: location, radius: r, identifier: identifier)
        // 2
        region.notifyOnEntry = true
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    func setMonitoringForMatch(_ match: Spiel){
        
        var localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = match.ort
        var localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                if let vc = UIApplication.shared.delegate?.window??.rootViewController{
                    vc.present(alertController, animated: true, completion: nil)
                }
                return
            }
            if match.number != nil && match.mannschaft != nil{
                self.startMonitoring(location: CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude), radius: 1000, identifier: match.mannschaft!+"_"+match.number!.description)
                self.matchesMonitored.append(match)
            }
            
        }
        
    }
    
    func startMonitoring(location: CLLocationCoordinate2D, radius:CLLocationDistance?, identifier: String) {
        // 1
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            //showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            print("Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            //showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
            print("Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = self.region(location: location, radius: radius, identifier: identifier)
        // 4
        locationManager.startMonitoring(for: region)
        print("Added "+identifier)
    }
    
    func stopMonitoring() {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier != "stried" else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("entered "+region.identifier)
        if region.identifier == "stried"{
            if matchesVisited.count == 0{
                OneSignalHelper.shared.enteredStried()
            }else{
                OneSignalHelper.shared.enteredStried(spiele: matchesVisited)
                matchesVisited = []
            }
        }else{
            let idSplitted = region.identifier.characters.split(separator: "_").map(String.init)
            if idSplitted.count == 2{
                let no = idSplitted[1]
                let mannschaft = idSplitted[0]
                if let spiel = matchesMonitored.first(where: {$0.mannschaft == mannschaft && $0.number == no}){
                    matchesVisited.append(spiel)
                }
                
            }
        }
    }
    
    
    
}
