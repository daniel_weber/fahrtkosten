//
//  SettingsTableViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 16.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

class SettingsTableViewController: UITableViewController {
    
    var userID:String?
    
    // MARK: Attributes
    var logoutButton: UIButton?
    
    @IBOutlet weak var firstTableViewCell: UIView!
    
    var profilePicture: UIView?
    var nameLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me")) { httpResponse, result in
            switch result {
            case .success(let response):
                if let userID = response.dictionaryValue?["id"] as? String{
                     let profile = UserProfile(userId: userID)
                    self.userID = userID
                    
                }
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
        AccessToken.current?.grantedPermissions
        connection.add(GraphRequest(graphPath: "/110703355663908?fields=access_token"))
        { httpResponse, result in
            switch result {
            case .success(let response):
                if let token = response.dictionaryValue?["access_token"] as? String{
                    self.getRoles(accessToken: token)
                }
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
        connection.start()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()*/
        reloadImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section{
        case 0:
            return 3
        case 1:
            if currentUser?.role == Role.admin{
                return 1
            }
            return 0
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch(indexPath.section, indexPath.row){
        case (0,0):
            return 100
        default:
            return tableView.rowHeight
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        switch(indexPath.section, indexPath.row){
        case (0,0):
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            if let pictureUrl = currentUser?.pictureUrl{
                let imageView = UIImageView(frame: CGRect.init(x: UIScreen.main.bounds.width/2-40, y: 8, width: 80, height: 80))
                imageView.layer.cornerRadius = 40
                self.loadImageFromUrl(url: pictureUrl, view: imageView)
                cell.addSubview(imageView)
                self.profilePicture = imageView
            }
        case (0,1):
            cell = tableView.dequeueReusableCell(withIdentifier: "labelCell", for: indexPath) as! LabelTableViewCell
            (cell as! LabelTableViewCell).label.text = currentUser?.fullName
        case (0,2):
            cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ButtonTableViewCell
            self.logoutButton = (cell as! ButtonTableViewCell).button
            self.logoutButton?.setTitle("Logout", for: .normal)
            self.logoutButton?.addTarget(self, action: #selector(self.logoutButtonClicked(_:)), for: .touchUpInside)
        case (1,0):
            cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ButtonTableViewCell
            (cell as! ButtonTableViewCell).button.setTitle("Fahrtkosten Löschen", for: .normal)
            (cell as! ButtonTableViewCell).button.addTarget(self, action: #selector(self.showDeleteAlert), for: .touchUpInside)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        }

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: Actions
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        
        let loginSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        //loginSheet.view.tintColor = UIColor(rgb: 0xeb3c96)
        let loginAction = UIAlertAction(title: "Logout", style: .destructive, handler: {_ in self.logoutButtonClicked()})
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        loginAction.setValue(UIImage(named:"facebookLogo"), forKey: "image")
        loginSheet.addAction(loginAction)
        loginSheet.addAction(cancelAction)
        self.present(loginSheet, animated: true, completion: nil)
    }
    
    func logoutButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logOut()
        WebService.shared.emailLogout({_ in })
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(self.showLoginSheet))
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
    }
    
    func showDeleteAlert(){
        let alert = UIAlertController(title: "Sicher?", message: "Wirklich alle Einträge archivieren", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: {_ in
            print("cancel")
        })
        let ok = UIAlertAction(title: "Archivieren", style: .destructive, handler: {_ in
            WebService.shared.archiveFahrtkosten({success, result in
                if !success{
                    let alert = UIAlertController(title: "Fehler", message: result as? String, preferredStyle: .alert)
                    let okay = UIAlertAction(title: "Okay", style: .default, handler: nil)
                    alert.addAction(okay)
                    self.present(alert, animated: true, completion: nil)
                }
            })
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    func loadImageFromUrl(url: String, view: UIImageView){
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    print("data: \(data)")
                    view.image = UIImage(data: data)
                    view.contentMode = .scaleAspectFill
                    view.clipsToBounds = true
                })
            }
        }
        
        // Run task
        task.resume()
    }
    
    func reloadImage(){
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me/picture?redirect=false&type=normal")) { httpResponse, result in
            switch result {
                case .success(let response):
                    if let data = response.dictionaryValue?["data"] as? [String: Any], let picUrl = data["url"] as? String{
                        currentUser?.pictureUrl = picUrl
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
                        }
                    }
                case .failed(let error):
                    print("Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
