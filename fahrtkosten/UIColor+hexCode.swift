//
//  UIColor+hexCode.swift
//  FeedBack
//
//  Created by Daniel Weber on 16.02.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    /**
     Creates a UIColor object for the given rgb value which can be specified
     as HTML hex color value. For example:
     
     let color = UIColor(rgb: 0x8046A2)
     let colorWithAlpha = UIColor(rgb: 0x8046A2, alpha: 0.5)
     
     - parameter rgb: color value as Int. To be specified as hex literal like 0xff00ff
     - parameter alpha: alpha optional alpha value (default 1.0)
     */
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((rgb & 0xff0000) >> 16) / 255
        let g = CGFloat((rgb & 0x00ff00) >>  8) / 255
        let b = CGFloat((rgb & 0x0000ff)      ) / 255
        
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    func getBrightness()->Double?{
        let uiC = self
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        if uiC.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let r = Double(fRed*255)
            let g = Double(fGreen*255)
            let b = Double(fBlue*255)
            let brightness: Double = sqrt(0.299*pow(r, 2)+0.587*pow(g, 2)+0.144*pow(b, 2))
            return brightness
        } else {
            return nil
        }        //return brightness
    }
    
}
