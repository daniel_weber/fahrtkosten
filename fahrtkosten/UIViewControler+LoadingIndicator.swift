//
//  UIViewControler+LoadingIndicator.swift
//  FeedBack
//
//  Created by Daniel Weber on 21.02.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

extension UIViewController{
    
    
    func addStatusBarBG(){
        let width = self.view!.frame.width
        let bg = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
        bg.backgroundColor = UIColor.red
        /*let frost = UIVisualEffectView(frame: bg.bounds)
        frost.effect = UIBlurEffect(style: .regular)
        frost.alpha = 0.5
        bg.addSubview(frost)*/
        self.view.addSubview(bg)
    }
    
    func showActivityIndicatory(){
        showActivityIndicatory(v: self.view)
    }
    
    
    func showActivityIndicatory(v: UIView) {
        
        var pos:CGFloat?
        if let pv = presentingViewController, let nc = pv.navigationController{
            pos = nc.navigationBar.frame.height
        }else{
            pos = 0
        }
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = v.bounds
        blurEffectView.alpha = 0.5
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.tag = 0xDEAD
        //v.addSubview(blurEffectView)
        
        
        var container: UIView = UIVisualEffectView(effect: blurEffect)
        container.frame = v.bounds
        container.alpha = 0.5
        //container.center = self.view.center
        //container.center.y -= pos!
        container.backgroundColor = UIColor(red:1, green:1, blue:1, alpha: 0)
        container.tag = 11
        
        var loadingView: UIView = UIView()
        loadingView.frame = CGRect.init(x: 0, y: 0, width: 80, height: 80)
        //loadingView.center = CGPoint.init(x:  UIScreen.main.nativeBounds.size.width / 2, y:  UIScreen.main.nativeBounds.size.height / 2)
        loadingView.center = CGPoint(x: UIScreen.main.bounds.size.width*0.5,y: UIScreen.main.bounds.size.height*0.5)
        //loadingView.center = self.view.center
        //loadingView.backgroundColor = UIColor(red:0.5, green:0.5, blue:0.5, alpha: 0.7)
        loadingView.backgroundColor = UIColor.red
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        loadingView.tag = 12
        
        var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        actInd.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        actInd.tag = 13
        
        v.addSubview(container)
        loadingView.addSubview(actInd)
        v.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    func hideActivityIndicatory(){
        if let actInd2 = view.viewWithTag(13) as? UIActivityIndicatorView{
            actInd2.stopAnimating()
            view.viewWithTag(13)?.removeFromSuperview()
            view.viewWithTag(12)?.removeFromSuperview()
            view.viewWithTag(11)?.removeFromSuperview()
            view.viewWithTag(0xDEAD)?.removeFromSuperview()
        }
    }
    
    func hideKeyboardWhenTappedAround()->UITapGestureRecognizer {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        return tap
    }
    
    func dontHideKeyboard(tap: UITapGestureRecognizer?){
        if tap != nil{
            view.removeGestureRecognizer(tap!)
        }
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
}
