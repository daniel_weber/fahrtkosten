//
//  PDFViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 27.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit

class PDFViewController: UIViewController, UIWebViewDelegate {
    // MARK: Attributes
    @IBOutlet weak var webView: UIWebView!
    var shareButton:UIBarButtonItem?
    
    let pdfUrl = "https://fahrtkosten.tvbeerfelden.de/v2/show_pdf.php"
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let shareButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action, target: self, action: #selector(self.loadPDFAndShare))
        self.shareButton = shareButton
        self.shareButton?.isEnabled = false
        self.navigationItem.rightBarButtonItem = self.shareButton
        webView.delegate = self
        webView.alpha = 0
        let request = URLRequest(url: URL(string: pdfUrl)!)
        //self.webView.loadRequest(request)
        self.getPDF()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: WebView Delegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        //showActivityIndicatory()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        hideActivityIndicatory()
        UIView.animate(withDuration: 0.5, animations: {
            webView.alpha = 1
        })
        savePdf()
        self.shareButton?.isEnabled = true
    }
    
    // MARK: Methods
    
    func getPDF(){
        showActivityIndicatory()
        
        var dataStr = "{"
        
        for (i,filter) in myFilters.enumerated(){
            if let searchStr = filter.searchString{
                dataStr += "\"\(filter.filterType.rawValue)\" : \"\(searchStr)\""
            }
            if i < myFilters.count-1{
                dataStr += ", "
            }
            
        }
        dataStr += "}"
        
        let url = URL(string: pdfUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/pdf", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        print(dataStr)
        request.httpBody = dataStr.data(using: .utf8)
        request.timeoutInterval = 60
        let session = URLSession.shared
        let task = session.downloadTask(with: request, completionHandler: {
            (location, response, error) in
            if error != nil {
                print(error!)
            } else {
                do {
                    let data = try Data(contentsOf: location!)
                    self.webView.load(data, mimeType: "application/pdf", textEncodingName: "", baseURL: location!.deletingLastPathComponent())
                } catch {
                    
                }
                
                
            }
        })
        task.resume()
    }
    
    func savePdf(){
        let fileManager = FileManager.default
        let fileName = "tvb_fahrtkosten_\(self.getCurrentDateString()).pdf"
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(fileName)
        let pdfDoc = NSData(contentsOf:URL(string: pdfUrl)!)
        fileManager.createFile(atPath: paths as String, contents: pdfDoc as Data?, attributes: nil)
    }
    
    func getCurrentDateString()->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.string(from: Date())
    }
    
    func loadPDFAndShare(){
        
        let fileManager = FileManager.default
        //let documentoPath = (self.getDirectoryPath() as NSString).appendingPathComponent("documento.pdf")
        let fileName = "tvb_fahrtkosten_\(self.getCurrentDateString()).pdf"
        let documentoPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(fileName)
        if fileManager.fileExists(atPath: documentoPath){
            let documento = NSData(contentsOfFile: documentoPath)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            present(activityViewController, animated: true, completion: nil)
        }
        else {
            print("document was not found")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
