//
//  SpielTableViewCell.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 10.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class SpielTableViewCell: MGSwipeTableCell {
    
    //MARK: Outlets
    @IBOutlet weak var matchLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var ortButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
