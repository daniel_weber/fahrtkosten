//
//  OneSignalHelper.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 28.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import Foundation
import OneSignal
import UserNotifications

class OneSignalHelper: NSObject, OSSubscriptionObserver{
    
    var playerId:String? = "2f67a0ea-b79b-4c1e-9871-70517e78b569"
    let appId = "92c13ef8-f374-47a1-95f6-a2e470eec1b2"
    
    private lazy var serviceRoot: String = {
        return "https://fahrtkosten.tvbeerfelden.de/v2"
    }()
    
    
    static public let shared = OneSignalHelper()
    
    // After you add the observer on didFinishLaunching, this method will be called when the notification subscription property changes.
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
            currentUser?.setOneSignalId(oneSignalId: playerId)
            WebService.shared.setOneSignalId(oneSignalID: playerId)
        }
        OneSignalHelper.shared.playerId = stateChanges.to.userId
    }
    
    func resendNotification(template: String, date: Date){
        
        let notificationData: Dictionary<String, Any> = [
            "app_id": appId,
            //"included_segments": ["All"],
            "include_player_ids": [currentUser?.oneSignalId],
            "template_id": template,
            "send_after": getDateString(date)
        ]
        OneSignal.postNotification(notificationData)
    }
    
    func enteredStried(spiele:[Spiel]){
        var spieleDict = [NSDictionary]()
        for spiel in spiele {
            spieleDict.append(spiel.dictionaryRepresentation())
        }
        backFromGame(at: Date.init(timeIntervalSinceNow: 70), userInfo: ["spiele":spieleDict])
    }
    
    /*func enteredStried(){
        let notificationData: Dictionary<String, Any> = [
            "app_id": appId,
            //"included_segments": ["All"],
            "include_player_ids": [currentUser?.oneSignalId],
            "template_id": "dfd5239d-bd23-4313-b985-b6ee4ac8cd6a",
            //"send_after": getDateString(date)
        ]
        
        OneSignal.postNotification(notificationData, onSuccess: {result in
            print("success")
            print(result)
        }, onFailure: {result in
            print("error")
            print(result) })
    }*/
    func enteredStried(){
        print("entered stried")
        self.backFromGame(at: Date.init(timeIntervalSinceNow: 70), userInfo: ["spiel": [spielplaene[0].spiele[0].dictionaryRepresentation()]])
    }
    
    func backFromGame(at date: Date, userInfo: Dictionary<AnyHashable, Any>?) {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(in: .current, from: date)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        
        let content = UNMutableNotificationContent()
        content.title = "Zurück vom Spiel?"
        content.body = "Du kommst gerade zurück vom Spiel?"
        content.sound = UNNotificationSound.default()
        content.setValue("YES", forKeyPath: "shouldAlwaysAlertWhileAppIsForeground")
        if userInfo != nil{
            content.userInfo = userInfo!
        }
        content.categoryIdentifier = "back_from_match"
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        let request = UNNotificationRequest(identifier: "back_from_match", content: content, trigger: trigger)
        print("added local notification to fire at: \(trigger.nextTriggerDate() ?? Date())")
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
        
    }
    
    private func getDateString(_ date: Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZ"
        print(formatter.string(from: date))
        return formatter.string(from: date)
    }
        
    func dicToJson(dic: Dictionary<String, String>) -> Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            print(String(data: jsonData, encoding: .utf8))
            return jsonData
        } catch {
            print(error.localizedDescription)
            return "{}".data(using: .utf8)!
        }
    }

}
