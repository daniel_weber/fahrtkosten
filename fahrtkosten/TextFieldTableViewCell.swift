//
//  TextFieldTableViewCell.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 20.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {
// MARK: Attributes
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
