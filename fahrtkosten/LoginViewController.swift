//
//  ViewController.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 07.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import OneSignal

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailLoginView: UIView!
    @IBOutlet weak var facebookLoginButton: UIButton!
    @IBOutlet weak var emailLoginButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.alpha = 0.5
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]*/
        
        let patternView = UIView(frame: self.view.bounds)
        patternView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "pattern"))
        
        hideKeyboardWhenTappedAround()
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        view.insertSubview(patternView, at: 1)
        view.bringSubview(toFront: self.facebookLoginButton)
        view.bringSubview(toFront: self.emailLoginButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === self.emailTextField{
            self.passwordTextField.becomeFirstResponder()
        }else if textField === self.passwordTextField{
            textField.resignFirstResponder()
            self.isEditing = false
            self.emailLogin(textField)
        }
        return false
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        switch result {
        case .failed(let error):
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        case .cancelled:
            print("User cancelled login.")
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            print("Logged in!")
            WebService.shared.setAccessToken(token: accessToken)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("logged out")
    }

    @IBAction func fbButtonClicked(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email, .custom("pages_show_list") ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                WebService.shared.setAccessToken(token: accessToken)
                print("Logged in!")
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabBarController") as! UITabBarController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
    }
    @IBAction func emailButtonclicked(_ sender: Any) {
        view.bringSubview(toFront: emailLoginView)
        UIView.animate(withDuration: 0.5, animations: {
            self.emailLoginButton.alpha = 0
            self.facebookLoginButton.alpha = 0
            self.emailLoginView.alpha = 1
        })
    }
    @IBAction func cancelEmailLogin(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.emailLoginButton.alpha = 1
            self.facebookLoginButton.alpha = 1
            self.emailLoginView.alpha = 0
        }, completion: {_ in
            self.view.sendSubview(toBack: self.emailLoginView)
        })
    }
    @IBAction func emailLogin(_ sender: Any) {
        if let email = self.emailTextField.text, let pwd = self.passwordTextField.text{
            WebService.shared.emailLogin(email: email, password: pwd, completion: {success, result in
                if success{
                    guard let root = result as? [String: Any], let accessToken = root["token"] as? [String: Any], let token = accessToken["token"] as? String, let expiresAt = accessToken["expiresAt"] as? String, let user = root["user"] as? [String: Any] else {
                        return
                    }
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    WebService.shared.setAccessToken(token: token, expiresAt: expiresAt)
                    WebService.shared.saveSecureString(token, forKey: "authenticationToken")
                    WebService.shared.saveSecureString(expiresAt, forKey: "expiringDate")
                    currentUser = User(id: user["id"] as? String, fullName: user["name"] as? String, firstName: nil, email: user["email"] as? String, pictureUrl: nil, oneSignalId: user["onesignalId"] as? String, role: .readOnly)
                    if let userType = user["userType"] as? String{
                        switch userType{
                            case "deprecated", "Super Administrator":
                            currentUser?.role = .admin
                            case "Publisher":
                            currentUser?.role = .author
                        default:
                            break
                        }
                    }
                    OneSignal.promptForPushNotifications(userResponse: { accepted in
                        print("User accepted notifications: \(accepted)")
                    })
                    DispatchQueue.main.async {
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabBarController") as! UITabBarController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = redViewController
                    }
                }else{
                    let alert = UIAlertController(title: "Fehler", message: result as? String, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
        
    }
    


}

