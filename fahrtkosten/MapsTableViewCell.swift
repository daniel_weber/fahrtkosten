//
//  MapsTableViewCell.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 10.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import UIKit
import MapKit

class MapsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var ortMapView: MKMapView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
