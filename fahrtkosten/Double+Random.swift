//
//  Double+Random.swift
//  CostCenters
//
//  Created by Daniel Weber on 26.04.17.
//  Copyright © 2017 zero2one. All rights reserved.
//

import Foundation

extension Double {
    private static let arc4randomMax = Double(UInt32.max)
    
    static func random0to1() -> Double {
        return Double(arc4random()) / arc4randomMax
    }
    
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func toEuro()->String?{
        return self.toEuro(2)
    }

    
    func toEuro(_ digits:Int)->String?{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = digits
        formatter.currencySymbol = "€"
        let num = NSNumber(value: self)
        return formatter.string(from: num)
    }
}
