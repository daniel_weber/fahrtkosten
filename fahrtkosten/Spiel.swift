//
//  Spiel.swift
//  fahrtkosten
//
//  Created by Daniel Weber on 10.06.17.
//  Copyright © 2017 tvbeerfelden. All rights reserved.
//

import Foundation

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Spiel {
    
        public var mannschaft : String?
        public var number : String?
        public var datum : String?
        public var zeit : String?
        public var heim : String?
        public var gast : String?
        public var tore : String?
        public var ort : String?
        
        /**
         Returns an array of models based on given dictionary.
         
         Sample usage:
         let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
         
         - parameter array:  NSArray from JSON dictionary.
         
         - returns: Array of Json4Swift_Base Instances.
         */
        public class func modelsFromDictionaryArray(array:NSArray) -> [Spiel]
        {
            var models:[Spiel] = []
            for item in array
            {
                models.append(Spiel(dictionary: item as! NSDictionary)!)
            }
            return models
        }
        
        /**
         Constructs the object based on the given dictionary.
         
         Sample usage:
         let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
         
         - parameter dictionary:  NSDictionary from JSON.
         
         - returns: Json4Swift_Base Instance.
         */
        required public init?(dictionary: NSDictionary) {
            
            mannschaft = dictionary["mannschaft"] as? String
            number = dictionary["number"] as? String
            datum = dictionary["datum"] as? String
            zeit = dictionary["zeit"] as? String
            heim = dictionary["heim"] as? String
            gast = dictionary["gast"] as? String
            tore = dictionary["tore"] as? String
            ort = dictionary["ort"] as? String
        }
        
        
        /**
         Returns the dictionary representation for the current instance.
         
         - returns: NSDictionary.
         */
        public func dictionaryRepresentation() -> NSDictionary {
            
            let dictionary = NSMutableDictionary()
            
            dictionary.setValue(self.mannschaft, forKey: "mannschaft")
            dictionary.setValue(self.number, forKey: "number")
            dictionary.setValue(self.datum, forKey: "datum")
            dictionary.setValue(self.zeit, forKey: "zeit")
            dictionary.setValue(self.heim, forKey: "heim")
            dictionary.setValue(self.gast, forKey: "gast")
            dictionary.setValue(self.tore, forKey: "tore")
            dictionary.setValue(self.ort, forKey: "ort")
            
            return dictionary
        }
    
    func getDate()->Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yy"
        if self.datum != nil {
            return formatter.date(from: self.datum!)
        }
        return nil
        
    }
    
    func isHeimspiel()->Bool{
        if self.heim != nil{
            if self.heim!.contains("Beerfelden") || self.heim!.contains("Mümling"){
                return true
            }
        }
        return false
    }
    
}
