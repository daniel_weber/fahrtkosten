//
//  FilterElement.swift
//  Pods
//
//  Created by Daniel Weber on 24.06.17.
//
//

import Foundation

enum FilterType{
    case mannschaft
    case hasBus
    case dateFrom
    case dateTo
}

var myFilters = [FilterElement]()

class FilterElement{
    
    let filterType: FilterType
    let searchString: String?
    let searchDate: Date?
    
    init?(filterType: FilterType, searchString: String?, searchDate: Date?){
        if (searchString == nil && filterType == .mannschaft) || (searchDate == nil && filterType != .mannschaft && filterType != .hasBus){
            return nil
        }
        
        self.filterType = filterType
        self.searchString = searchString
        self.searchDate = searchDate
    }
    
}
